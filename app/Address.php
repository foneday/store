<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cart;

class Address extends Model
{

	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'active', 'zipcode', 'housenumber', 'streetaddress_1', 'streetaddress_2', 'city', 'country'
    ];

    /**
     * Get the user that that belongs to the address.
     */
    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public static function getVerzendkosten($country = null)
    {
            if($country == 'NL') {

                if(Cart::total() > 150) {
                    return 0.00;
                } else {
                    return 4.95;
                }

            } elseif($country == 'BE') {

                 if(Cart::total() > 250) {
                    return 0.00;
                } else {
                    return 9.95;
                }

            } elseif($country == 'DE') {

                 if(Cart::total() > 250) {
                    return 0.00;
                } else {
                    return 9.95;
                }

            } else {

                 if(Cart::total() > 450) {
                    return 0.00;
                } else {
                    return 14.95;
                }

            }

    }

    
}
