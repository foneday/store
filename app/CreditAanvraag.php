<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditAanvraag extends Model
{

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'opmerking', 'hoeveel', 'status', 'redenvanafwijzing'
    ];


     /**
     * Get the user that that belongs to the address.
     */
    public function user()
    {
    	return $this->belongsTo('App\User');
    }

}
