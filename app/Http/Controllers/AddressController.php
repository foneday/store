<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;
use App\Country;
use Auth;

class AddressController extends Controller
{

	public function index()
	{

		$addresses = Address::where('user_id', Auth::user()->id)->orderBy('active', 'desc')->get();

		$countries = Country::all();

		return view('logged.page.dashboard.addresscontrol')->with(compact('addresses', 'countries'));

	}

	public function switchStatus(Request $request)
	{

		switch($request->type) {
			case "red":
				$active = 1;
				break;
			case "green":
				$active = 0;	
				break;
		}

		$address = Address::find($request->id);
		$address->active = $active;
		$address->save();

		return response()->json(array('id' => $request->id, 'status' => $active));

	}

	public function delete(Request $request)
	{

		$id = $request->id;

		$address = Address::find($id);

		if($address->delete()) {
					return response()->json($id);
		} else {
					return response()->json('error');
		}

	}

	public function create(Request $request)
	{

		$address = new Address();
		$address->user_id = Auth::user()->id;
		$address->active = 1;
		$address->name = $request->name;
		$address->country = $request->country;
		$address->zipcode = $request->postcode;
		$address->housenumber = $request->huisnummer;
		$address->streetaddress_1 = $request->adres;
		$address->city = $request->dorp;
		$address->save();

		return redirect(route('addresscontrol'));

	}
    
}
