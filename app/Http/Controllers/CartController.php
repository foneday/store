<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductHistory;
use App\Pvariation;
use App\Product;
use Response;
use Cart;
use Auth;
use URL;

class CartController extends Controller
{

	public function index()
	{
			$history = ProductHistory::where('user_id', Auth::user()->id)->take(5)->get();

		$products = [];

		foreach($history as $history)
		{

			$product = Product::where('id', $history->product_id)->first();
			array_push($products, $product);

		}

		return view('public.page.cart.cart')->with(compact('products'));

	}

	 /**
     * Show overview of cart
     *
     * @return void
     */
	public function overview()
	{

		echo '<a href="/clearcart">Clearcart</a>';

		dd(Cart::content());

	}


	 /**
     * Clear cart
     *
     * @return void
     */
	public function clearCart()
	{

		Cart::destroy();

		return Response::json(array(
		    'count' => '0',
		    'total' => '0.00',
		    'cartcontent' => view('public.etc.cart-content')->render(),
        ));

	}


	 /**
     * Add a product to the cart
     *
     * @return void
     */
	public function addToCart(Request $request)
	{

		$sku = $request->sku;

		if($product = Product::where('sku', $sku)->first()) {
 			$name = $product->name;
 			$price = $product->price;
 			$discount = $product->discount;
 			if(!empty($image = $product->pimage()->first())) {
				$image = '/storage/'.$product->pimage()->first()->url.''; 
			} else {
				$image = 'images/products/placeholder.png';
			}
		} else {
			$product = Pvariation::where('sku', $sku)->first();
			$info = Product::find($product->product_id);
			$name = $info->name;
			$price = $product->price;
			$discount = $product->discount;
			if(!empty($image = $product->pimage()->first())) {
				$image = '/storage/'.$product->pimage()->first()->url.''; 
			} else {
				$image = 'images/products/placeholder.png';
			}
		}

		Cart::add($sku, $name, $request->amount, $price, ['discount' => $discount, 'image' => $image, 'pid' => $product->id]);

		$count = Cart::count();
		$total = Cart::total();

      	return Response::json(array(
		    'count' => $count,
		    'total' => $total,
		    'cartcontent' => view('public.etc.cart-content')->render(),
        ));
	
	}

	 /**
     * Delete certain product from cart without Ajax
     *
     * @return void
     */
	public function deleteFromCartWA($rowId)
		{

		    Cart::remove($rowId);

	      	return redirect(URL::previous());
		
		}
    

   	 /**
     * Delete certain product from cart
     *
     * @return void
     */
	public function deleteFromCart(Request $request)
	{

		$rowId = $request->rowid;

	    Cart::remove($rowId);

		$count = Cart::count();
		$total = Cart::total();

      	return Response::json(array(
		    'count' => $count,
		    'total' => $total,
		    'cartcontent' => view('public.etc.cart-content')->render(),
        ));
	
	}
    //
}
