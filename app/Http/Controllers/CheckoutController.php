<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;
use App\Order;
use Auth;

class CheckoutController extends Controller
{

	public function confirmation($id)
	{

		$order = Order::find($id);

		if($order->user_id != Auth::user()->id) {
			dd('Er is iets fout gegaan.');
		}

		return view('public.page.checkout.confirm')->with(compact('order'));

	}

	public function shipment()
	{

		$addresses = Address::where([
			['user_id', '=', Auth::user()->id],
			['active', '=', 1]
		])->get();

		return view('public.page.checkout.shipment')->with(compact('addresses'));

	}

	public function mollieCallback()
	{

			$shipment = Shipment::newShipment($request->address, $request->koerier, $order_number, $order_id); // Moet naar Callback


	}

	public function payment(Request $request)
	{

		// dd($request->bezorgmethode);



		if($request->bezorgmethode == 'post') {

			$address = Address::find($request->address);

			$verzendkosten = $address->getVerzendkosten($address->country);

		} elseif($request->bezorgmethode == 'ophalen') {

			$verzendkosten = 0;

		} elseif($request->bezorgmethode == 'pakketpunt') {

			$verzendkosten = 4.95;

		} else {
			return 'Er is iets fout gegaan.';
		}

		return view('public.page.checkout.payment')->with(compact('verzendkosten', 'request', 'address'));

	}
    //
}
