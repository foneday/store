<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CreditAanvraag;
use Auth;

class CreditAanvraagController extends Controller
{

	public function credit()
	{

		$kredietaanvraag = CreditAanvraag::where([
			['user_id', '=', Auth::user()->id],
			['status', '=', '0']
		])->first();

		return view('logged.page.dashboard.credit')->with(compact('kredietaanvraag'));

	}

	public function requestCredit(Request $request)
	{

		$kredietaanvraag = new CreditAanvraag();
		$kredietaanvraag->user_id = Auth::user()->id;
		$kredietaanvraag->status = 0;
		$kredietaanvraag->opmerking = $request->opmerkingen;
		$kredietaanvraag->hoeveel = $request->limiet;
		$kredietaanvraag->save();

		return redirect(URL(route('creditcontrol')));

	}
    //
}
