<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class CustomLoginController extends Controller
{
	public function loginUser(Request $request)
    
	    {
	    	$email	       = $request->email;
	    	$password      = $request->password;
	    	$rememberToken = $request->remember;
	    	// now we use the Auth to Authenticate the users Credentials
			// Attempt Login for members
			if (Auth::attempt(['email' => $email, 'password' => $password])) {
				$msg = array(
					'status'  => 'success'
				);
				return response()->json($msg);
			} else {
				$msg = array(
					'status'  => 'error'
				);
				return response()->json($msg);
			}
	    }

}
