<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shipment;
use App\Order;
use Auth;

class DashboardController extends Controller
{

	public function index()
	{

		$lastorders = Order::where([
			['user_id', '=', Auth::user()->id],
			['status', '=', 1]
		])->get()->take(3);

		$status = new Shipment();

		return view('logged.page.dashboard.dashboard')->with(compact('lastorders', 'status'));

	}

	public function invoices()
	{

		return view('logged.page.dashboard.invoices');

	}
    //
}
