<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductHistory;
use App\Product;
use Auth;

class FrontController extends Controller
{

	public function index()
	{

		$history = ProductHistory::where('user_id', Auth::user()->id)->take(5)->get();

		$products = [];

		foreach($history as $history)
		{

			$product = Product::where('id', $history->product_id)->first();
			array_push($products, $product);

		}

		return view('public.page.frontpage.frontpage')->with(compact('products'));

	}
    //
}
