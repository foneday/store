<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Imports\ProductImport;
use Storage;

class ImportController extends Controller
{

	public function import(Request $request)
	{

		// $file = public_path('map.xltx');

		$path = $request->file('bestand')->store('bestand');

		Excel::import(new ProductImport, $path);
		
	}

	public function form()
	{

		return view('form');

	}
    //
}
