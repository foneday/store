<?php

namespace App\Http\Controllers;

use Mollie\Laravel\Facades\Mollie;
use Illuminate\Http\Request;
use App\OrderItem;
use App\Shipment;
use App\Checkout;
use App\Address;
use App\Order;
use Response;
use Cart;
use Auth;
use DB;

class OrderController extends Controller
{

	public function webhook(Request $request)
	{

		$payment_id = $request->id;

		$payment = Mollie::api()->payments()->get($payment_id);

		$order = Order::where('payment_id', $payment_id)->first();

		$address = $order->address;

		$order_id = $order->id;

		if ($payment->isPaid())
		{
		    $order->status = 1;
		    $order->paid = true;
		    $shipment = Shipment::newShipment($address, $order->shipment_method, $order->order_number, $order_id);
		    $order->parcel_id = $shipment->id;
			$order->trackandtrace = $shipment->tracking_number;
	}

	}

	public function checkOrder($id)
	{

		$result = false;

		$order = Order::find($id);

		if($order->user_id != Auth::user()->id) {
			dd('Er is iets fout gegaan.');
		}

		if($order->status == 1) {

			$result = true;

		}

		return Response::json( $result );

	}

	public function newOrder(Request $request)
	{

		$betaalmethode = $request->betaalmethode;

		$order_number = mt_rand(10000000, 99999999);

		$order_id = Order::newBasicOrder($request, $order_number); // Nieuwwe order maken

		$order = Order::find($order_id); // Vind het nieuwe order

		$order->shipping_method = $request->bezorgmethode;

		// ///////////////////////////////////// //
		// Producten in cart toevoegen aan order //
		// ///////////////////////////////////// //

		foreach(Cart::content() as $cart) {
				OrderItem::addOrderItem($order_id, $cart->options->pid, $cart->qty, $cart->price, $cart->options->discount);
		}

		if($request->bezorgmethode == 'post') {

				$total = (Cart::total() * 1.21) + Address::getVerzendkosten(Address::find($request->address)->country);
			    $totalrounded = number_format((float)$total, 2, '.', '');

		} else {

			$total = Cart::total() * 1.21;
			$totalrounded = number_format((float)$total, 2, '.', '');

		}

		if($request->bezorgmethode == 'post') {

					$address = Address::find($request->address);
					$order->zipcode = $address->zipcode;
					$order->housenumber = $address->housenumber;
					$order->streetaddress_1 = $address->streetaddress_1;
					$order->city = $address->city;
					$order->country = $address->country;
					$order->address = $request->address;

		}


		if($betaalmethode == 'ideal' || $betaalmethode == 'paypal') {

					    $payment = Mollie::api()->payments()->create([
					    'amount' => [
					        'currency' => 'EUR',
					        'value' => ''.$totalrounded.'', // You must send the correct number of decimals, thus we enforce the use of strings
					    ],
					    "description" => ''.$order_number.'',
					    'method' => $betaalmethode,
					    'redirectUrl' => Route('confirmation', ['id' => $order_id]),
					    'webhookUrl'   => route('paymentwebhook'),
					    ]);

					    $payment = Mollie::api()->payments()->get($payment->id);

					    $order->payment_id = $payment->id;

					    // redirect customer to Mollie checkout page
					    return redirect($payment->getCheckoutUrl(), 303);

						}  elseif($betaalmethode == 'krediet') {

							if($request->bezorgmethode == 'post') {

								$shipment = Shipment::newShipment($request->address, $request->koerier, $order_number, $order_id);
								$order->parcel_id = $shipment->id;
								$order->trackandtrace = $shipment->tracking_number;

							}

					}
		
		$order->save();

		return redirect(Route('confirmation', ['id' => $order_id]));

	}
    //
}
