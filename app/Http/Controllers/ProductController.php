<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductHistory;
use App\Pvariation;
use App\Product;
use App\Pbrand;
use Auth;   

class ProductController extends Controller
{

	 /**
     * Products
     *
     * @return void
     */
    public function showProducts(Request $request, $brandid = null)
    {

        $products = Product::where('activate', 1);

        if($brandid != null) {
            $products = $products->where('pbrand_id', $brandid);
        }

        $products = $products->paginate(6);

        $brands = \App\Pbrand::all();
        $colors = \App\Pcolor::all();

        if ($request->ajax()) {

            $products = Product::where('activate', 1);

            // ->where('name', 'like', '%' . $request->get('search') . '%')
            

            if($request->get('brands')) {
                $products = $products->whereIn('pbrand_id', $request->get('brands'));
            }

            if($request->get('search')) {
                $products = $products->where('name', 'like', '%' . $request->get('search') . '%');
            }

            if($request->get('sort')) {

            }

            if($request->get('colors')) {
                $prods = array();
                $pvariations = Pvariation::whereIn('pcolor_id', $request->get('colors'))->get();
                foreach($pvariations as $vari) {
                    array_push($prods, $vari->product_id);
                }
                $products = $products->whereIn('id', $prods);
            }
        
            $products = $products->paginate(6);

            return view('public.page.products.presult', compact('products'));

        }

        return view('public.page.products.products', compact('products', 'brands', 'colors', 'brandid'));
        
    }

    /**
     * Show a product
     *
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {

        $product = Product::where('slug', $slug)->first();

        ProductHistory::addHistory(Auth::user()->id, $product->id);

        return view('public.page.products.product', compact('product'));

    }

    /**
     * Get color data of a product
     *
     */
    public function getColorData($id)
    {

        $pvariation = Pvariation::find($id);

        $pimages = $pvariation->pimage()->get();

        return response()->json(array('price' => $pvariation->price, 'sku' => $pvariation->sku, 'discount' => $pvariation->discount, 'images' => $pimages));

  }


    //
}
