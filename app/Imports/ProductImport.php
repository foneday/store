<?php

namespace App\Imports;

use App\Product;
use Storage;
use App\Pimage;
use App\Pvariation;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManager;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;    
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {

            if(!empty($row)) {

            $sku = $row['sku'];
            $colorcode = substr($sku, -1);
            $images = explode('|',$row['image']);
            $filenames = explode('|',$row['file']);


            // dd($colorcode);

            if($colorcode == 'X') {

                $product = new Product();
                $product->sku = $sku;
                $product->name = $row['title'];

                switch($row['cat']) {
                    case 'Parts':
                    $cat = 2;
                    break;

                    case 'Housings' :
                    $cat = 3;
                    break;

                    case 'Batteries' :
                    $cat = 4;
                    break;

                    case 'Accessoires' :
                    $cat = 5;
                    break;

                    case 'Displays' :
                    $cat = 1;
                    break;
                }
                
                $product->pcategorie_id = $cat;
                $product->price = $row['price'];
                $product->activate = 1;
                $product->save();

                foreach($filenames as $file) {

                    $image = new Pimage();
                    $image->title = $row['title'];
                    $image->product_id = $product->id;
                    $image->url = $file;
                    $image->save();

                }

            } else {

                $cleansku = substr($sku, 0, -1);
                $product = Product::where('sku', $sku)->first();
                if($product) {



                } else {

                if(empty(Product::where('sku', $cleansku)->first())) {

                    $product = new Product();
                    $product->sku = $cleansku;
                    $product->name = $row['title'];

                    switch($row['cat']) {
                        case 'Parts':
                        $cat = 2;
                        break;

                        case 'Housings' :
                        $cat = 3;
                        break;

                        case 'Batteries' :
                        $cat = 4;
                        break;

                        case 'Accessoires' :
                        $cat = 5;
                        break;

                        case 'Displays' :
                        $cat = 1;
                        break;
                    }
                    
                    $product->pcategorie_id = $cat;
                    $product->price = $row['price'];
                    $product->activate = 1;
                    $product->save();

                } else {
                    $product = Product::where('sku', $cleansku)->first();
                }

                    $pvariation = new Pvariation();
                    $pvariation->product_id = $product->id;
                    $pvariation->sku = $sku;

                    switch($row['cat']) {
                        case 'Parts':
                        $cat = 2;
                        break;

                        case 'Housings' :
                        $cat = 3;
                        break;

                        case 'Batteries' :
                        $cat = 4;
                        break;

                        case 'Accessoires' :
                        $cat = 5;
                        break;

                        case 'Displays' :
                        $cat = 1;
                        break;
                    }
                
                    $pvariation->price = $row['price'];
                    $pvariation->activate = 1;



                    if (strpos($colorcode,'W') !== false) {
                        $pvariation->pcolor_id = 1;

                    } elseif (strpos($colorcode,'S') !== false) {
                        $pvariation->pcolor_id = 2;

                    } elseif (strpos($colorcode,'E') !== false) {
                        $pvariation->pcolor_id = 3;

                    } elseif (strpos($colorcode,'B') !== false) {
                        $pvariation->pcolor_id = 4;

                    } elseif (strpos($colorcode,'R') !== false) {
                        $pvariation->pcolor_id = 5;

                    } elseif (strpos($colorcode,'U') !== false) {
                        $pvariation->pcolor_id = 6;

                    } elseif (strpos($colorcode,'D') !== false) {
                        $pvariation->pcolor_id = 6;

                    } elseif (strpos($colorcode,'H') !== false) {
                        $pvariation->pcolor_id = 6;    

                    } elseif (strpos($colorcode,'Y') !== false) {
                        $pvariation->pcolor_id = 7;

                    } elseif (strpos($colorcode,'F') !== false) {
                        $pvariation->pcolor_id = 7;

                    } elseif (strpos($colorcode,'C') !== false) {
                        $pvariation->pcolor_id = 8;

                    } elseif (strpos($colorcode,'N') !== false) {
                        $pvariation->pcolor_id = 8;

                    } elseif (strpos($colorcode,'J') !== false) {
                        $pvariation->pcolor_id = 8;

                    } elseif (strpos($colorcode,'V') !== false) {
                        $pvariation->pcolor_id = 9;

                    } elseif (strpos($colorcode,'L') !== false) {
                        $pvariation->pcolor_id = 9;

                    } elseif (strpos($colorcode,'P') !== false) {
                        $pvariation->pcolor_id = 10;

                    } elseif (strpos($colorcode,'K') !== false) {
                        $pvariation->pcolor_id = 10;

                    } elseif (strpos($colorcode,'G') !== false) {
                        $pvariation->pcolor_id = 11;

                    } elseif (strpos($colorcode,'A') !== false) {
                        $pvariation->pcolor_id = 12;

                    } elseif (strpos($colorcode,'I') !== false) {
                        $pvariation->pcolor_id = 12;

                    } elseif (strpos($colorcode,'O') !== false) {
                        $pvariation->pcolor_id = 13;

                    } else {
                        $pvariation->pcolor_id = 14;
                    }


                    $pvariation->save();

                foreach($filenames as $file) {

                    $image = new Pimage();
                    $image->title = $row['title'];
                    $image->product_id = $product->id;
                    $image->pvariation_id = $pvariation->id;
                    $image->url = $file;
                    $image->save();

                }



                }

            }

            foreach($images as $image) {

                if(!empty($image)) {

                    $filename = basename($image);

                    Image::make($image)->save(public_path('images/products/' . $filename));


                }

            }

   
            }






        }
    }
}
