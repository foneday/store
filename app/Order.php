<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cart;
use Auth;

class Order extends Model
{

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id'
    ];

    

     /**
     * Get the history that belong to the user.
     */
    public function orderitems()
    {
        return $this->hasMany('App\OrderItem');
    }

    public static function newBasicOrder($request, $order_number)
    {

        $order = new Order();
        $order->user_id = Auth::user()->id;
        $order->payment_method = $request->betaalmethode;
        $order->status = 0;
        $order->shipping_method = $request->verzendmethode;
        $order->address = $request->address;
        $order->order_number = $order_number;
        $order->payment_method = $request->betaalmethode;
        $order->save();

        return $order->id;

    }
    //
}
