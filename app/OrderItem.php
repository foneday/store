<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'product_id', 'amount', 'price', 'discount'
    ];

    public static function addOrderItem($order_id, $product_id, $amount, $price, $discount)
    {

        $orderitem = new OrderItem();
        $orderitem->order_id = $order_id;
        $orderitem->product_id = $product_id;
        $orderitem->amount = $amount;
        $orderitem->price = $price;
        $orderitem->discount = $discount;
        $orderitem->save();
        
    }

	 /**
     * Get the order that that belongs to the orderitem.
     */
    public function Order()
    {
    	return $this->belongsTo('App\Order');
    }
    //
}

