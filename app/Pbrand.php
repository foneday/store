<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pbrand extends Model
{

	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'name', 'activate'
    ];

    /**
     * Get the products that the brand belongs to.
     */
    public function products()
    {

        return $this->hasMany('App\Product');

    }

    //
}
