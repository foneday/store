<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pcategorie extends Model
{

	 /**
     * Get the product that belongs to the image.
     */
    public function product()
    {

    	return $this->belongsTo('App\Product');

    }
    //
}
