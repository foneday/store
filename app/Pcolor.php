<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pcolor extends Model
{

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'code'
    ];
    

     /**
     * Get the variations that that belong to the color.
     */
    public function pvariation()
    {
        return $this->hasMany('App\Pvariation');
    } 
    
}
