<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Nova\Fields\Text;

class Pimage extends Model
{

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'pvariation_id', 'title', 'description', 'url'
    ];

    /**
     * Get the product that belongs to the image.
     */
    public function product()
    {

    	return $this->belongsTo('App\Product');

    }

    /**
     * Get the variation that belongs to the image.
     */
    public function pvariation()
    {

        return $this->belongsTo('App\Pvariation');

    }
    
    //
}
