<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Product extends Model
{

    use HasSlug;

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'short_description', 'sku', 'price', 'discount', 'active', 'pbrand_id'
    ];

    /**
     * Get the images that that belong to the product.
     */
    public function pimage()
    {
    	return $this->hasMany('App\Pimage');
    } 

    /**
     * Get the variations that that belong to the product.
     */
    public function pvariation()
    {
        return $this->hasMany('App\Pvariation');
    } 

    /**
     * Get the variations that that belong to the product.
     */
    public function pcategorie()
    {
        return $this->hasMany('App\Pcategorie');
    } 

    /**
     * Get if a variation exists for this product
     */
    public function hasVariation()
    {

        return $this->hasMany('App\Pvariation')->get()->isNotEmpty();

    }

    /**
     * Get the brand that belongs to the product.
     */
    public function brand()
    {
        return $this->belongsTo('App\Pbrand', 'pbrand_id')->withDefault([
            'name' => 'Onbekend'
        ]);
    }

    //
}
