<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ProductHistory extends Model
{

	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'product_id'
    ];

	 /**
     * Get the user that that belongs to the address.
     */
    public function user()
    {
    	return $this->belongsTo('App\User');
    }

	 /**
     * Add new history record
     */
    public static function addHistory($userid, $productid) {

    	$producthistory = ProductHistory::where('product_id', $productid)->first();
    	if(!empty($producthistory)) {
    		$producthistory->delete();
    	}

    	$producthistory = new ProductHistory();
    	$producthistory->user_id = $userid;
    	$producthistory->product_id = $productid;
    	$producthistory->save();

    }
    //
}
