<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pvariation extends Model
{

	/**
    * The attributes that are mass assignable.
    *
    * @var array
	*/
	protected $fillable = [
		'product_id', 'pcolor_id', 'sku', 'sku', 'price', 'discount', 'activate'
	];

	 /**
     * Get the product that belongs to the variation.
     */
    public function product()
    {

    	return $this->belongsTo('App\Product');

    }

	 /**
     * Get the color that belongs to the variation.
     */
    public function pcolor()
    {

    	return $this->belongsTo('App\Pcolor');

    }

     /**
     * Get the images that that belong to the variation.
     */
    public function pimage()
    {
        return $this->hasMany('App\Pimage');
    } 

    //
}
