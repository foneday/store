<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Address;
use Auth;

class Shipment extends Model
{

	public static function getStatus($id)
	{

		$connection = new \Picqer\Carriers\SendCloud\Connection('3c985cac724341c4bac53b427dc2bd20', 'a430e4ff2ab64abf9958c656fa30a2a9');
		$sendCloud = new \Picqer\Carriers\SendCloud\SendCloud($connection);
		$parcel = $sendCloud->parcels()->find($id);

		$status = '';

		switch($parcel->status['id']) {
			case "1000":
				$status = "Klaar om te versturen";
				break;
		}

		return $status;

	}

	public static function newShipment($address, $koerier, $order_number, $order_id)
	{	
		$connection = new \Picqer\Carriers\SendCloud\Connection('3c985cac724341c4bac53b427dc2bd20', 'a430e4ff2ab64abf9958c656fa30a2a9');
		$sendCloud = new \Picqer\Carriers\SendCloud\SendCloud($connection);

		$parcel = $sendCloud->parcels();

		$address = Address::where([
			['user_id', '=', Auth::user()->id],
			['id', '=', $address]
		])->first();

		$add = ''.$address->streetaddress_1.' '.$address->housenumber.'';

		$order = Order::find($order_id);	

		if($koerier == 'postnl') {
				$shipment_method = 323;
				$order->shipment_method == "PostNL";
		} elseif($koerier == 'ups') {
				$shipment_method = 137;
				$order->shipment_method == "UPS";
		}	

		$parcel->name = Auth::user()->name;
		$parcel->company_name = Auth::user()->companyname;
		$parcel->address = $add;
		$parcel->city = $address->city;
		$parcel->postal_code = $address->zipcode;
		$parcel->country = $address->country;
		$parcel->requestShipment = true;
		$parcel->shipment = $shipment_method; // Shipping method, get possibilities from $sendCloud->shippingMethods()->all()
		$parcel->order_number = $order_number;

		$shipment = $parcel->save();

		$order->shipment_method = $koerier;
		$order->trackandtrace = $shipment->tracking_number;
		$order->parcel_id = $shipment->id;

		$order->save();

		return $parcel;

	}
    //
}
