<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Lab404\Impersonate\Models\Impersonate;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles; 
    use Impersonate;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the adresses that belong to the user.
     */
    public function addresses()
    {
        return $this->hasMany('App\Address');
    }

    /**
     * Get the crediet requests that belong to the user.
     */
    public function creditrequest()
    {
        return $this->hasMany('App\CreditAanvraag');
    }

     /**
     * Get the history that belong to the user.
     */
    public function histories()
    {
        return $this->hasMany('App\ProductHistory');
    }
    
}
