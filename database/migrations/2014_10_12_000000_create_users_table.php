<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            
            $table->increments('id');

            $table->integer('parent_id')->nullable(); // ID voor parent user

            $table->string('name');

            $table->string('email')->unique();

            $table->string('email-finance')->nullable();

            $table->string('password');

            $table->boolean('credit')->default(false);

            $table->integer('creditlimit')->nullable();

            // Persoonlijke gegevens

            $table->string('firstname');

            $table->string('lastname');

            $table->string('companyname');

            $table->integer('companynumber');

            $table->string('vatnumber');

            $table->rememberToken();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
