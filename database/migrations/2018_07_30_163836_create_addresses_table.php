<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('user_id');

            $table->boolean('active');

            $table->string('name');

            $table->string('zipcode');

            $table->integer('housenumber');

            $table->string('streetaddress_1');

            $table->string('streetaddress_2')->nullable();

            $table->string('city');

            $table->string('country');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('addresses');

    }
}
