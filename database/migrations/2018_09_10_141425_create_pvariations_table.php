<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePvariationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pvariations', function (Blueprint $table) {

            $table->increments('id');

            $table->decimal('discount',9,2)->nullable();

            $table->integer('product_id');

            $table->integer('pcolor_id');

            $table->string('sku')->nullable();

            $table->decimal('price',9,2)->nullable();

            $table->boolean('activate');

            $table->string('quality')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pvariations');
    }
}
