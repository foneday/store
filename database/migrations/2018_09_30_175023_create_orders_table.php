<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('user_id');

            $table->integer('order_number');

            $table->string('status');

            $table->string('trackandtrace')->nullable();

            $table->string('shipping_method')->nullable();

            $table->integer('payment_id')->nullable();

            $table->string('shipment_method')->nullable();

            $table->integer('parcel_id')->nullable();

            $table->integer('address')->nullable();

            $table->string('payment_method'); // Mollie of factuur

            $table->boolean('paid')->nullable();

            $table->integer('ophaalstatus')->nullable(); // 1 = In proces 2 = Klaar voor ophalen 3 = Opgehaald

            $table->string('zipcode')->nullable();

            $table->integer('housenumber')->nullable();

            $table->string('streetaddress_1')->nullable();

            $table->string('city')->nullable();

            $table->string('country')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
