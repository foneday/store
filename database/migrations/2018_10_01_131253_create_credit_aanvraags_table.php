<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditAanvraagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_aanvraags', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('user_id');

            $table->string('opmerking')->nullable();

            $table->integer('hoeveel');

            $table->integer('status');  // 0 = in behandeing 1 = toegewezen 2 = afgewezen

            $table->string('redenvanafwijzing')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_aanvraags');
    }
}
