<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            $table->increments('id');

            $table->string('name')->nullable();

            $table->string('slug');

            $table->longText('description')->nullable();

            $table->mediumText('short_description')->nullable();

            $table->string('sku')->nullable()->unique();

            $table->decimal('price',9,2)->nullable();

            $table->decimal('discount',9,2)->nullable();

            $table->boolean('activate')->default(0);

            $table->integer('pbrand_id')->nullable();

            $table->integer('pcategorie_id')->default(1);

            $table->string('quality')->nullable();

            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
