<?php

use Illuminate\Database\Seeder;
use App\Pcategorie;

class CategorieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$categorie = new Pcategorie();
    	$categorie->title = 'Displays';
    	$categorie->save(); 

    	$categorie = new Pcategorie();
    	$categorie->title = 'Parts';
    	$categorie->save(); 

    	$categorie = new Pcategorie();
    	$categorie->title = 'Housings';
    	$categorie->save(); 

    	$categorie = new Pcategorie();
    	$categorie->title = 'Batteries';
    	$categorie->save(); 

    	$categorie = new Pcategorie();
    	$categorie->title = 'Accessoires';
    	$categorie->save(); 
        //
    }
}
