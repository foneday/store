<?php

use Illuminate\Database\Seeder;
use App\Country;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	// Nederland
    	$country = new Country();
    	$country->name = "Nederland";
    	$country->code = "NL";
    	$country->save(); 

    	// Duitsland
    	$country = new Country();
    	$country->name = "Deutschland";
    	$country->code = "DE";
    	$country->save(); 
    
    }
}
