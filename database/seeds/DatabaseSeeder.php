<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(UsersTableSeeder::class);
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(BrandSeeder::class);
        $this->call(PcolorSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(CategorieSeeder::class);
        $this->call(DeviceSeeder::class);

    }
    
}
