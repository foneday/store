<?php

use Illuminate\Database\Seeder;
use App\Pdevice;

class DeviceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$device = new Pdevice();
    	$device->title = "Smartphone";
    	$device->save();

    	$device = new Pdevice();
    	$device->title = "Tablet";
    	$device->save();

    	$device = new Pdevice();
    	$device->title = "Iphone";
    	$device->save();

    	$device = new Pdevice();
    	$device->title = "Ipad";
    	$device->save();

    	$device = new Pdevice();
    	$device->title = "Smartwatch";
    	$device->save();


        //
    }
}
