<?php

use Illuminate\Database\Seeder;

class PcolorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	DB::table('pcolors')->insert([
            'name' => 'Wit',
            'code' => 'FFFFFF',
            'shortcode' => 'W',
        ]);

        DB::table('pcolors')->insert([
            'name' => 'Zilver',
            'code' => 'C0C0C0',
            'shortcode' => 'S',

        ]);

        DB::table('pcolors')->insert([
            'name' => 'Grijs',
            'code' => '808080',
            'shortcode' => 'E',
        ]);

        DB::table('pcolors')->insert([
            'name' => 'Zwart',
            'code' => '000000',
            'shortcode' => 'B',
        ]);

        DB::table('pcolors')->insert([
            'name' => 'Rood',
            'code' => 'C52B1E',
            'shortcode' => 'R',
        ]);

        DB::table('pcolors')->insert([
            'name' => 'Blauw',
            'code' => '01689B',
            'shortcode' => 'U',
        ]);

        DB::table('pcolors')->insert([
            'name' => 'Geel',
            'code' => 'F9E11E',
            'shortcode' => 'Y',
        ]);

        DB::table('pcolors')->insert([
            'name' => 'Groen',
            'code' => '39870S',
            'shortcode' => 'N',
        ]);

        DB::table('pcolors')->insert([
            'name' => 'Paars',
            'code' => '42145F',
            'shortcode' => 'L',
        ]);

        DB::table('pcolors')->insert([
            'name' => 'Roze',
            'code' => 'F092CD',
            'shortcode' => 'P',
        ]);

        DB::table('pcolors')->insert([
            'name' => 'Goud',
            'code' => 'D4AF37',
            'shortcode' => 'G',
        ]);

        DB::table('pcolors')->insert([
            'name' => 'Bruin',
            'code' => '673372',
            'shortcode' => 'A',
        ]);

        DB::table('pcolors')->insert([
            'name' => 'Oranje',
            'code' => 'E17000',
            'shortcode' => 'O',
        ]);
        //
    }
}
