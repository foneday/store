<?php

use Illuminate\Database\Seeder;
use App\Product;
use App\Pvariation;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Creation of demo products

        $product = new Product([
            'name' => 'iPad Pro 12.9 Display Assembly (Complete with Mainboard) White',
            'sku' => 'XXXXXXXXXX',
            'price' => '23.39',
            'pbrand_id' => '1',
            'activate' => 1
        ]);

        $product->save();

        $product = new Product([
            'name' => 'iPad Pro 12.9 Display Assembly (Complete with Mainboard) White',
            'sku' => 'BBBBBBBBBBB',
            'price' => '23.39',
            'pbrand_id' => '1',
            'activate' => 1
        ]);

        $product->save();

        $product = new Product([
            'name' => 'iPad Pro 12.9 Display Assembly (Complete with Mainboard) White',
            'sku' => 'XXX1XXXXXX',
            'price' => '23.39',
            'pbrand_id' => '1',
            'activate' => 1
        ]);

        $product->save();

        $product = new Product([
            'name' => 'iPad Pro 12.9 Display Assembly (Complete with Mainboard) White',
            'sku' => 'CCCCCCCCCCC',
            'price' => '23.39',
            'pbrand_id' => '1',
            'activate' => 1
        ]);

        $product->save();

        $product = new Product([
            'name' => 'iPad Pro 12.9 Display Assembly (Complete with Mainboard) White',
            'sku' => 'DDDDD3DDDDD',
            'price' => '23.39',
            'pbrand_id' => '1',
            'activate' => 1
        ]);

        $product->save();


        $product = new Product([
            'name' => 'iPad Pro 12.9 Display Assembly (Complete with Mainboard) White',
            'sku' => 'DDDDDDD4DDD',
            'price' => '23.39',
            'pbrand_id' => '1',
            'activate' => 1
        ]);

        $product->save();


        $product = new Product([
            'name' => 'iPad Pro 12.9 Display Assembly (Complete with Mainboard) White',
            'sku' => 'DDD5DDDDDDD',
            'price' => '23.39',
            'pbrand_id' => '1',
            'activate' => 1
        ]);

        $product->save();


        $product = new Product([
            'name' => 'iPad Pro 12.9 Display Assembly (Complete with Mainboard) White',
            'sku' => 'DDDD44DDDDD',
            'price' => '23.39',
            'pbrand_id' => '1',
            'activate' => 1
        ]);

        $product->save();


        $product = new Product([
            'name' => 'iPad Pro 12.9 Display Assembly (Complete with Mainboard) White',
            'sku' => 'DDDDXXDDDDD',
            'price' => '23.39',
            'pbrand_id' => '1',
            'activate' => 1
        ]);

        $product->save();

        // Creation of demo product variations
        
        $pvariation = new Pvariation();
        $pvariation->product_id = 1;
        $pvariation->sku = '12345678';
        $pvariation->pcolor_id = 1;
        $pvariation->price = "10.12";
        $pvariation->discount = "9.00";
        $pvariation->activate = 1;
        $pvariation->save();

        $pvariation = new Pvariation();
        $pvariation->product_id = 1;
        $pvariation->sku = '12345678';
        $pvariation->pcolor_id = 2;
        $pvariation->price = "10.12";
        $pvariation->discount = "9.00";
        $pvariation->activate = 1;
        $pvariation->save();

        
    }
}
