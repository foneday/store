<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class RolesAndPermissionsSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	// Create list of permissions
    	Permission::create(['name' => 'dashboard']);
    	Permission::create(['name' => 'products']);
    	Permission::create(['name' => 'users']);

    	// Permissions for administrator
	    $permissions_administrator = [
	        'products', 'users'
	    ];

	    // Clear permission cache of Spatie
    	app()['cache']->forget('spatie.permission.cache');

    	// Create developer role and assign all permissions
    	$developer = Role::create(['name' => 'developer']);
    	$developer->givePermissionTo(Permission::all());

    	// Create administrator role and assign all permissions minus developer permissions.
    	$administrator = Role::create(['name' => 'administrator']);
    	$administrator->givePermissionTo($permissions_administrator);

    	// Assign developer role to master account created in UsersTableSeeder
    	$user = User::find('1');
    	$user->assignRole('developer'); // Assign developer role to master account

        //
    }
}
