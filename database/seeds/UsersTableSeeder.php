<?php

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;
use App\User;
use App\Address;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            'name' => 'Stijn Bakker',
            'email' => 'contact@steensolutions.nl',
            'password' => bcrypt('secret'),
            'firstname' => 'Stijn',
            'lastname' => 'Bakker',
            'companyname' => 'Steen Solutions',
            'companynumber' => '4324232',
            'vatnumber' => 'NL01332131232'
        ]);

        $address = new Address();
        $address->user_id = 1;
        $address->active = 1;
        $address->name = "Steen Solutions";
        $address->zipcode = "6828AS";
        $address->housenumber = 20;
        $address->streetaddress_1 =  "Spoorwegstraat";
        $address->city = "Arnhem";
        $address->country = "NL";
        $address->save();

    }
    
}
