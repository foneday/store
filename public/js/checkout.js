/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 54);
/******/ })
/************************************************************************/
/******/ ({

/***/ 54:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(55);


/***/ }),

/***/ 55:
/***/ (function(module, exports) {

$(function () {
	$('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function () {
		$(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
		$(this).addClass('selected');
		$(this).find('input[type="radio"]').prop("checked", true);
	});
});

$(document).on('click', '#addresspick', function () {
	$('.checkout-button').show();
});

$(document).on('click', '.post', function () {
	$('#checkout-adres').show();
	$('#checkout-ophalen').hide();
	$('#checkout-pickup').hide();
});

$(document).on('click', '.pakketpunt', function () {
	$('#checkout-adres').hide();
	$('#checkout-ophalen').hide();
	$('.checkout-button').show();
	$('#checkout-pickup').show();
});

$(document).on('click', '.ophalen', function () {
	$('#checkout-adres').hide();
	$('#checkout-ophalen').show();
	$('.checkout-button').show();
	$('#checkout-pickup').hide();
});

$(document).on('click', '.paymentknop', function () {
	$('.btn-pay').show();
});

$('#test').click(function () {
	checkorder($(this).data('id'));
});

function checkorder(id) {

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$.ajax({
		url: "/checkorder/" + id,
		type: "POST",
		dataType: 'json',
		success: function success(data) {
			if (data == true) {
				$('.circle-loader').addClass('load-complete');
				$('.checkmark').show();
				$('#verwerktexttogo').hide();
				$('.btn-bekijk-bestelling').show();
				$('#verwerktextdone').show();
			} else {
				$('.circle-loader').removeClass('load-complete');
				$('.checkmark').hide();
				$('#verwerktexttogo').show();
				$('.btn-bekijk-bestelling').hide();
				$('#verwerktextdone').hide();
			}
		},
		error: function error(msg) {
			console.log(msg);
		}
	});
}

setInterval(function () {
	checkorder($('.order_id').val());
}, 2000);

$(document).on('click', '.btn-pay', function () {
	$(this).attr("disabled", true);
	$(this).parents('form').submit();
});

/***/ })

/******/ });