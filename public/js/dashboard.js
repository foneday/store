/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 48);
/******/ })
/************************************************************************/
/******/ ({

/***/ 48:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(49);


/***/ }),

/***/ 49:
/***/ (function(module, exports) {

$(document).ready(function () {
	$('[data-toggle="tooltip"]').tooltip();
});

$('#deleteModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget);
	var adresid = button.attr('data-adres-id');
	var modal = $(this);
	modal.find('#delete').val(adresid);
});

$(document).ready(function () {
	$(document).on('click', '.greendot, .reddot', function (e) {
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			url: "/dashboard/adresbeheer/status",
			method: 'post',
			data: {
				id: $(this).attr('data-id'),
				type: $(this).attr('data-type')
			},
			success: function success(result) {
				switchStatus(result['id'], result['status']);
			}
		});
	});
});

function switchStatus(id, type) {
	var dot = $('#dot-' + id + '');

	switch (type) {
		case 0:
			dot.removeClass('greendot').addClass('reddot');
			dot.attr('data-type', 'red').attr('data-original-title', 'Adres non-actief');
			$('#address-' + id + '').attr('data-active', '0');
			break;
		case 1:
			dot.removeClass('reddot').addClass('greendot');
			dot.attr('data-type', 'green').attr('data-original-title', 'Adres actief');
			$('#address-' + id + '').attr('data-active', '1');
			break;
	}
	sortAddresses();
}

function sortAddresses() {
	$('#addresses .address').sort(function (a, b) {
		return $(b).attr('data-active') - $(a).attr('data-active');
	}).appendTo('#addresses');
}

// alert($(this).data('type'));

$(document).ready(function () {
	$('#delete').click(function (e) {
		e.preventDefault();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			url: "/dashboard/adresbeheer/delete",
			method: 'post',
			data: {
				id: $("#delete").val()
			},
			success: function success(result) {
				if (result != 'error') {
					$('#address-' + result + '').fadeOut(900, function () {
						$(this).remove();
					});
					$('#deleteModal').modal('toggle');
				} else {
					console.log('Error');
				}
			}
		});
	});
});

/***/ })

/******/ });