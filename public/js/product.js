/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 50);
/******/ })
/************************************************************************/
/******/ ({

/***/ 50:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(51);


/***/ }),

/***/ 51:
/***/ (function(module, exports) {

// Colomn sorting

$(document).on('click', '.switch-1', function () {
	switchCol(1);
});
$(document).on('click', '.switch-2', function () {
	switchCol(2);
});
$(document).on('click', '.switch-3', function () {
	switchCol(3);
});

function switchCol(col) {
	var amount = 12 / col;
	$('.product-col').each(function () {
		$(this).removeClass();
		$(this).addClass('product-col col-sm-' + amount + '');
	});
}

// Ajax product paginatie

$(window).on('hashchange', function () {
	if (window.location.hash) {
		var page = window.location.hash.replace('#', '');
		if (page == Number.NaN || page <= 0) {
			return false;
		} else {
			getData(page);
		}
	}
});

$(document).ready(function () {
	$(document).on('click', '.pagination a', function (event) {
		event.preventDefault();
		$('li').removeClass('active');
		$(this).parent('li').addClass('active');
		var myurl = $(this).attr('href');
		var page = $(this).attr('href').split('page=')[1];
		getData(page);
	});
});

function getData(page) {
	$.ajax({
		url: '?page=' + page,
		type: "get",
		datatype: "html"
	}).done(function (data) {
		$("#content-row").empty().html(data);
		location.hash = page;
	}).fail(function (jqXHR, ajaxOptions, thrownError) {
		alert('No response from server');
	});
}

// Image switch

$(document).on('click', '.next', function () {

	var number = $(this).data('count');

	var product = $(this).parent();
	var count = product.children('.product-image').length;

	if (count == number) {

		var parent = $(this).parent();
		var first = parent.children('.first');
		var last = parent.children('.last');
		last.removeClass('active');
		first.addClass('active');
		$(this).data('count', 1);
	} else {

		var newnumber = number + 1;

		$(this).data('count', newnumber);

		var activeclass = $(this).parent().find('.active').removeClass('active');
		var next = activeclass.next('.product-image');
		next.addClass('active');
	}

	console.log(number);
});

// Color switch


$(document).on('click', '.color', function () {

	var data_id = $(this).data('variationid');
	var element = this;

	$.ajax({
		url: '/getcolordata/' + data_id,
		type: "get",
		datatype: "html"
	}).done(function (data) {
		var price = data.price.replace(".", ",");

		$(element).parent().children('.picked').removeClass('picked');
		$(element).addClass('picked');

		var parent = $(element).parent().parent();

		var datasku = parent.parent().children('#product-nav').children('span').children('.btn-add-cart');

		datasku.data('sku', data.sku);

		if (data.discount == null) {
			parent.find('.discount').hide();
			parent.find('.normal-price').html('€ ' + price + '');
		} else {
			var discount = data.discount.replace(".", ",");
			parent.find('.discount').show();
			parent.find('.discount').html('€ ' + price + '');
			parent.find('.normal-price').html('€ ' + discount + '');
		}

		if (data.images == '') {
			var html = '<img src="/images/products/placeholder.png" class="product-image active">';
		} else {
			var html = '<a class="prev">&#10094;</a><a class="next" data-count="1">&#10095;</a>';
			var length = data.images.length;
			$.each(data.images, function (i, image) {
				if (i === 0) {
					html += '<img src="/images/products/' + image.url + ' " class="product-image active first">';
				} else if (i == length - 1) {
					html += '<img src="/images/products/' + image.url + ' " class="product-image last">';
				} else {
					html += '<img src="/images/products/' + image.url + ' " class="product-image">';
				}
			});
		}

		parent.parent().children('.pimages').html(html);
	}).fail(function (jqXHR, ajaxOptions, thrownError) {
		alert('No response from server');
	});
});

var brands = [];
var colors = [];

$(document).on('click', '.brand-filter', function () {
	brands.length = 0;
	filter();
});

$(document).on('click', '.color-filter', function () {
	colors.length = 0;
	filter();
});

$(document).on('keyup', '#searchinput', function () {
	filter();
});

$(document).on('change', '.sorting-form', function () {
	filter();
});

function filter() {

	$('.loading').show();

	$(".brand-filter:checked").each(function () {
		brands.push($(this).data('id'));
	});

	$(".color-filter:checked").each(function () {
		colors.push($(this).data('id'));
	});

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$.ajax({
		url: "/producten",
		data: {
			brands: brands,
			search: $('#searchinput').val(),
			colors: colors,
			sort: $('.sorting-form').val()
		},
		type: "post",
		success: function success(data) {
			$('#content-row').html(data);
			$('.loading').hide();
		},
		error: function error(msg) {
			console.log(msg);
		}
	});
}

/***/ })

/******/ });