
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

window.$ = window.jQuery = require('jquery');

require("jquery-ui");

require('./bootstrap');

window.Vue = require('vue');
 
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});


var loadingOverlay = document.querySelector('.loading');
function toggleLoading(event){
  if (event.keyCode !== 13) return;
  
  document.activeElement.blur();
  
  if (loadingOverlay.classList.contains('hidden')){
    loadingOverlay.classList.remove('hidden');
  } else {
    loadingOverlay.classList.add('hidden');
  }
}

document.addEventListener('keydown', toggleLoading);

$(document).ready(function($) {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 17) {
            $('#logo').addClass('small-logo');
            $('.navbar-main').addClass('box-shadow');
        } else {
            $('#logo').removeClass('small-logo');
            $('.navbar-main').removeClass('box-shadow');
        }
    });
});

$('#recipeCarousel').carousel({
  interval: 10000
})

// $('.carousel .carousel-item').each(function(){
//     var next = $(this).next();
//     if (!next.length) {
//     next = $(this).siblings(':first');
//     }
//     next.children(':first-child').clone().appendTo($(this));
    
//     if (next.next().length>0) {
//     next.next().children(':first-child').clone().appendTo($(this));
//     }
//     else {
//       $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
//     }
// });

$('.carousel .carousel-item').each(function(){
    var next = $(this).next();
    if (!next.length) {
    next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));

    for (var i=0;i<2;i++) {
        next=next.next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }

        next.children(':first-child').clone().appendTo($(this));
      }
});
