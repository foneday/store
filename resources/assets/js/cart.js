// CART

$(document).on('click', '.btn-add-cart', function(e) {

	e.preventDefault(); 

	$('.loading').show();

	var sku_data = $(this).data('sku');
	var amount_data = $(this).parent().parent().children('span').children('.amount').val();
	if(amount_data == undefined) {
		var amount_data = $('.ammount').val();
	}

			setInterval(function(){ 
			   // toggle the class every 
			   $('.fa-shopping-cart').addClass('jiggle');  
			   setTimeout(function(){
			     // toggle another class
			    $('.fa-shopping-cart').removeClass('jiggle');
			   },1000)

			},1000);

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
	$.ajax({
		url: "/add-to-cart",
		type: "POST",
		data: {sku: sku_data, amount: amount_data},
		dataType: 'json',
		success: function(data) {
			$('.aantal').html(data.count);
			$('.total-amount').html('€ '+data.total+'');
			$('#center-cart').html(data.cartcontent);
			$('.loading').hide();

		},
		error: function(msg){
			console.log(msg);
		}
	});
});

$(document).on('click', '.cart-clear', function(e) {

	$('.loading').show();

	e.preventDefault(); 

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
	$.ajax({
		url: "/clearcart",
		type: "POST",
		dataType: 'json',
		success: function(data) {
			$('.aantal').html(data.count);
			$('.total-amount').html('€ '+data.total+'');
			$('#center-cart').html(data.cartcontent);
			$('.loading').hide();
		},
		error: function(msg){
			console.log(msg);
		}
	});
});


$(document).on('click', '.delete-row', function(e) {
	e.preventDefault(); 
	var rowid = $(this).data('id');
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
	$.ajax({
		url: "/delete-from-cart",
		type: "POST",
		data: {rowid: rowid},
		dataType: 'json',
		success: function(data) {
			$('.aantal').html(data.count);
			$('.total-amount').html('€ '+data.total+'');
			$('#center-cart').html(data.cartcontent);

		},
		error: function(msg){
			console.log(msg);
		}
	});
});

