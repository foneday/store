$(function(){
	$('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function(){
		$(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
		$(this).addClass('selected');
		$(this).find('input[type="radio"]').prop("checked", true);
		
	});
});

$(document).on('click', '#addresspick', function() {
	$('.checkout-button').show();
});

$(document).on('click', '.post', function() {
	$('#checkout-adres').show();
	$('#checkout-ophalen').hide();
	$('#checkout-pickup').hide();
});

$(document).on('click', '.pakketpunt', function() {
	$('#checkout-adres').hide();
	$('#checkout-ophalen').hide();
    $('.checkout-button').show();
	$('#checkout-pickup').show();
});

$(document).on('click', '.ophalen', function() {
	$('#checkout-adres').hide();
	$('#checkout-ophalen').show();
    $('.checkout-button').show();
	$('#checkout-pickup').hide();
});    

$(document).on('click', '.paymentknop', function() {
	$('.btn-pay').show();
});


$('#test').click(function() {
         checkorder($(this).data('id'));
});

function checkorder(id) {

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
	$.ajax({
		url: "/checkorder/" + id,
		type: "POST",
		dataType: 'json',
		success: function(data) {
				if(data == true) {
					   $('.circle-loader').addClass('load-complete');
  					   $('.checkmark').show();
  					   $('#verwerktexttogo').hide();
  					   $('.btn-bekijk-bestelling').show();
  					   $('#verwerktextdone').show();
				} else {
					   $('.circle-loader').removeClass('load-complete');
  					   $('.checkmark').hide();
  					   $('#verwerktexttogo').show();
  					   $('.btn-bekijk-bestelling').hide();
  					   $('#verwerktextdone').hide();
				}
		},
		error: function(msg){
			console.log(msg);
		}
	});

}



setInterval(function() {
  checkorder($('.order_id').val());
}, 2000);

$(document).on('click', '.btn-pay', function() {
	$(this).attr("disabled", true);
	$(this).parents('form').submit();
});