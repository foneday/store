$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

$('#deleteModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) 
  var adresid = button.attr('data-adres-id') 
  var modal = $(this)
  modal.find('#delete').val(adresid); 
});

$(document).ready(function(){
	$(document).on('click', '.greendot, .reddot', function(e) {
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			url: "/dashboard/adresbeheer/status",
			method: 'post',
			data: {
				id: $(this).attr('data-id'),
				type: $(this).attr('data-type')
			},
			success: function(result){
				switchStatus(result['id'], result['status']);
			}
		});
	});
});

function switchStatus(id, type)
{
	var dot = $('#dot-'+ id +'');

	switch (type) {
		case 0:
			dot.removeClass('greendot').addClass('reddot');
			dot.attr('data-type', 'red').attr('data-original-title', 'Adres non-actief');
			$('#address-'+ id +'').attr('data-active', '0');
		break;
		case 1:
			dot.removeClass('reddot').addClass('greendot');
			dot.attr('data-type', 'green').attr('data-original-title', 'Adres actief');
			$('#address-'+ id +'').attr('data-active', '1');
		break;
	}
	sortAddresses();
}

function sortAddresses()
{
	$('#addresses .address').sort(function(a, b) {
    	return $(b).attr('data-active') - $(a).attr('data-active');
     }).appendTo('#addresses');
}

// alert($(this).data('type'));

$(document).ready(function(){
	$('#delete').click(function(e) {
		e.preventDefault();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			url: "/dashboard/adresbeheer/delete",
			method: 'post',
			data: {
				id: $("#delete").val()
			},
			success: function(result){
				if(result != 'error') {
					$('#address-'+result+'').fadeOut(900, function() { $(this).remove(); });
					$('#deleteModal').modal('toggle');
				} else {
					console.log('Error');
				}
			} 
		});
	});
});