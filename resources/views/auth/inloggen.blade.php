@extends('public.layout.structure.public-app-empty')

@section('content')


<img id="logo" src="{{ asset('images/branding/foneday-logo.svg') }}">

<div id="blueline"></div>

<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <!-- Icon -->
    <div class="fadeIn first">
    </div>

                <div class="container">
                      <div class="alert alert-danger" style="display: none;" id="error" role="alert">
                         Deze gegevens kunnen niet worden gevonden in onze database. Controleer de gegevens en probeer het opnieuw.
                      </div>
                </div>

    <!-- Login Formulier -->
    <form method="POST" onsubmit="return LoginUser()" aria-label="{{ __('Login') }}">

    @csrf

      <input id="email" type="email" class="fadeIn second{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="E-mail adres" value="{{ old('email') }}" required autofocus>

      <input id="password" type="password" class="fadeIn third{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Wachtwoord" name="password" required>

      <button type="submit" class="fadeIn fourth inloggen">Inloggen</button>

    </form>

     <div id="forgot-link" class="text-left">
     	<a href="#">Wachtwoord vergeten?</a>
     </div>

  </div>

  <div id="infoContent">

  	<div>
  		<img id="lock-icon" src="{{ asset('images/icons/locked.svg') }}">
  		FoneDay maakt gebruik van een beveiligde HTTPS-verbinding met als doel het veilig kunnen verwerken van gegevens. Deze verbinding is te herkennen aan het groene slotje in de adres-balk. Druk hier voor meer informatie betreft de beveiliging van FoneDay.
  	</div>

  </div>

</div>

<div>

	<a href="/" class="btn btn-success btn-back"><span class="far fa-home"></span> Terug naar FoneDay</a>

</div>

@endsection


@section('js')

<script>
        function LoginUser()
        {
            $('.inloggen').html('<span class="fas fa-spinner fa-spin"></span>').prop('disabled', true);
            var token    = $("input[name=_token]").val();
            var email    = $("input[name=email]").val();
            var password = $("input[name=password]").val();
            var data = {
                _token:token,
                email:email,
                password:password
            };
            // Ajax Post 
            $.ajax({
                type: "post",
                url: "/login/user",
                data: data,
                cache: false,
                success: function (data)
                {
                    if(data.status == 'success') {
                      window.location.replace("{{ route('frontpage') }}");
                    } else {
                        $('.inloggen').html('Inloggen').prop('disabled', false);;
                        $( "#error" ).fadeIn( "slow", function() {  });
                    }
                },
                error: function (data){
                   $( "#error" ).fadeIn( "slow", function() {  });
                }
            });
            return false;
        }

</script>
@endsection