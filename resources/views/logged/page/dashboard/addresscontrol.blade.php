@extends('public.layout.structure.public-app-store')

@section('content')

    <div id="addresscontrol" class="container">

        <div class="row">

              @include('logged.page.dashboard.navigation')

            <div class="col-8">

                <div id="dashboard-card" class="card">
                    <div class="card-body">

                            <h4 class="title">Adressen</h4>

                            <p>U heeft hier de mogenlijkheid om een of meerdere adressen op te slaan en deze later te gebruiken in het bestelproces.</p>

                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newModal">Een nieuw adres toevoegen</button>
        
                    </div>
                </div>

                

                <div id="addresses" class="row">

                    @foreach($addresses as $address)

                    <div id="address-{{ $address->id }}" data-active="{{ $address->active }}" class="col-4 address">

                        <div id="dashboard-card" class="card">
                            <div class="card-header align-items-center">

                                @if($address->active == 0)
                                    <span id="dot-{{ $address->id }}" data-toggle="tooltip" data-id="{{ $address->id }}" data-type="red" title="Adres non-actief" class="reddot"></span>
                                @elseif($address->active == 1)
                                    <span id="dot-{{ $address->id }}" data-toggle="tooltip" data-id="{{ $address->id }}" data-type="green" title="Adres actief" class="greendot"></span>
                                @endif

                                <span id="nav" class="float-right"> &nbsp; <a href="#" data-toggle="modal" data-target="#deleteModal" data-adres-id="{{ $address->id }}"><span class="fas fa-times"></a></span></span>

                            </div>

                            <div class="card-body">

                                <span>{{ Auth::user()->companyname }}</span><br />
                                <span>{{ $address->streetaddress_1 }}</span> <br />
                                <span>{{ $address->zipcode }} {{ $address->city }}, {{ $address->country }}</span> <br />

                            </div>
                        </div>
                        
                    </div>

                    @endforeach

                </div>

            </div>


        </div>
    </div>

        {{-- Delete adres modal --}}

        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                   <div class="modal-header">
                       <h5 class="modal-title" id="deleteModalLabel">Weet u zeker dat dit adres moet worden verwijderd?</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                  <div class="modal-footer">
                  <form>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="delete" type="button" type="submit" value="" class="btn btn-danger">Verwijder adres</button>
                  </form>
                  </div>
                </div>
              </div>
            </div>

            {{-- Nieuw adres modal --}}

            <div class="modal fade" id="newModal" tabindex="-1" role="dialog" aria-labelledby="newModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="newModalLabel">Een nieuw adres toevoegen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">

                    <form method="post" action="{{ route('newadres') }}">

                      @csrf

                      <div class="form-group">

                        <div class="form-group">
                          <label for="name">Naam van adres</label>
                          <input required="required" class="form-control" name="name">
                        </div>

                        <div class="form-group">
                          <label for="country">Land</label>
                          <select required="required" class="form-control" name="country">
                            @foreach($countries as $country)
                              <option value="{{ $country->code }}">{{ $country->name }}</option>
                            @endforeach
                          </select>
                        </div>

                        <label for="postcode">Postcode + huisnummer: </label><br />

                        <div class="form-group postcode-field">
                          <input required="required" class="form-control" name="postcode" placeholer="">
                        </div> 

                        <div class="form-group huisnummer-field">
                          <input required="required" class="form-control" name="huisnummer" placeholer="">
                        </div>

                        <div class="form-group">
                          <label for="adres">Adres</label>
                          <input required="required" class="form-control adres-field" placeholer="Adres" name="adres">
                        </div>

                        <div class="form-group">
                          <label for="dorp">Dorp / stad</label>
                          <input required="required" class="form-control dorp-field" placeholer="Dorp" name="dorp">
                        </div>

                      </div>
                  <button type="submit" class="btn btn-primary">Adres opslaan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                    </form>

                  </div>
                </div>
              </div>
            </div>

@endsection

@section('js')

<script src="{{ asset('js/dashboard.js') }}" defer></script>

@endsection
