@extends('public.layout.structure.public-app-store')

@section('content')

    <div class="container">

        <div class="row">

            <div class="col-12">

                <div id="dashboard-card" class="card">

                    <div class="card-body">

                        Advertenties

                    </div>

                </div>

            </div>

        </div>

        <div class="row">

                    @include('logged.page.dashboard.navigation')

            <div class="col-8">

                <div id="dashboard-card" class="card">
                    <div class="card-body">

                            <h4 class="title">Op rekening bestellen</h4>

                            <p>Bij FoneDay bieden wij de mogelijkheid om als klant op rekening te bestellen. Bestellingen kunnen gemakkelijk en veilig achteraf worden betaald en verwerkt in u administratie.</p>

                            @if(Auth::user()->credit == 0)

                            <p>Op dit moment heeft u geen toestemming om op rekening te bestellen. Om op rekening te bestellen dient u eerst een aanvraag te doen, dit kan door het volgende formulier in te vullen:</p>

                            @else

                            <p>U heeft op dit moment een krediet van: € {{ Auth::user()->creditlimit }} euro.  Indien u het krediet limiet wilt verlagen of verhogen, gebruik dan het onderstaande formulier:</p>


                            @endif

                            <b>Kredietaanvraag</b>

                            @if($kredietaanvraag == null)



                             <form id="creditrequest-form" method="post" action="{{ route('creditaanvraag') }}">

                                @csrf

                                <div class="form-group">
                                    <label for="limiet">Gewenst krediet</label>
                                    <input class="form-control" name="limiet" placeholder="Bedrag in euro's" type="number" required>
                                </div>

                                <div class="form-group">
                                    <label for="opmerkingen">Opmerkingen over de aanvraag</label>
                                    <textarea class="form-control" id="opmerkingen" name="opmerkingen" rows="3" placeholder="Opmerkingen"></textarea>
                                </div>

                                <div class="form-check">
                                  <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" value="" required>
                                    Ik ga akkoord met toetsing van mijn gegevens bij kredietinstancies ter ondersteuning van deze kredietaanvraag.
                                  </label>
                                </div>

                                <br />

                                <button class="btn btn-success" type="submit">Aanvraag versturen</button>

                            </form>

                            </form>

                            @else

                            <p>Er is al een kredietaanvraag in behandeling met de volgende gegevens: </p>
                            
                            <p>
                            <b>Limiet:</b> € {{ $kredietaanvraag->hoeveel }} euro<br />
                            <b>Opmerkingen:</b> {{ $kredietaanvraag->opmerking }} <br />
                            <b>Status:</b> In behandeling <br /></p>

                            <p>Indien deze gegevens niet juist zijn, neem dan contant op met onze klantenservice. Het kan tot 24 uur duren voordat u een reactie ontvangt op uw aanvraag.
 
                            @endif
    
                    </div>

                </div>

            </div>


        </div>

    </div>

@endsection
