@extends('public.layout.structure.public-app-store')

@section('content')

    <div class="container">

        <div class="row">

            <div id="dashboard-card" class="card">
            
                <div class="card-body">
            
                    @include('parts.productslider')
            
                </div>
            
            </div>

        </div>

        <div class="row">

                    @include('logged.page.dashboard.navigation')

            <div class="col-8">

                <div id="lastorders" class="row">


                @foreach($lastorders as $lastorder)
                    <div class="col-4">
                        <div class="card">
                            <div class="card-body">

                         @if($lastorder->shipping_method == 'post')
                                @if($lastorder->shipment_method == 'postnl')
                                <img class="postal-logo" src="{{ asset('images/branding/post-nl-icon.svg') }}">
                                @elseif($lastorder->shipment_method == 'ups')
                                <img class="postal-logo" src="{{ asset('images/branding/ups-icon.svg') }}">
                                @endif
                        @endif
                                <span class="order-number">#{{ $lastorder->order_number }}</span>
                                <span class="order-date">24-04-2018</span>

                                <hr class="line-1" />

                                <div class="order-products">
                                   <img src="{{ asset('images/example/samsung_1.jpg') }}">
                                   <img src="{{ asset('images/example/samsung_1.jpg') }}">
                                   <img src="{{ asset('images/example/samsung_1.jpg') }}">
                                </div>

                                <div class="order-info">
                                    @if($lastorder->shipping_method == 'post')

                                    <span class="verzendtype">Levermethode: {{ $lastorder->shipment_method }}</span><br />
                                    <span class="verzendstatus">Status: {{ $status->getStatus($lastorder->parcel_id) }}</span><br />
                                    <span class="tracking">Traceer dit pakket</span>

                                    @elseif($lastorder->shipping_method == 'ophaalpunt') 

                                    @elseif($lastorder->shipping_method == 'ophalen')

                                    <span class="verzendtype">Levermethode: Ophalen</span><br />
                                    <span class="verzendstatus">Status: Opgehaald</span><br />

                                    @endif 


                                </div>

                                <hr class="line-2" />

                            </div>
                        </div>
                    </div>

                    @endforeach

                </div>


                <div id="dashboard-card" class="card">
                    <div class="card-body">

                            <h4 class="title">Betalingsoverzicht</h4>

                            U vindt hieronder een overzicht van openstaande en reeds betaalde facturen.

                            <div class="payment-header">

                                <div class="row">

                                    <div class="col-2">Factuur</div>
                                    <div class="col-2">Datum</div>
                                    <div class="col-2">Bedrag</div>
                                    <div class="col-2">Verval</div>
                                    <div class="col-2">Openstaand</div>
                                    <div class="col-2">Betalen</div>

                                </div>

                            </div>

                            <div class="payment-rule">

                                <div class="row payment-row">

                                    <div class="col-2">#2017-1021</div>
                                    <div class="col-2">27-06-2018</div>
                                    <div class="col-2">€ 322,32</div>
                                    <div class="col-2">07-07-2018</div>
                                    <div class="col-2 payment-row-status not-paid">Status</div>
                                    <div class="col-2"><span class="fas payment-row-pay fa-credit-card"></span></div>

                                </div>

                            </div>
        
                    </div>
                </div>

                <br />

            </div>


        </div>

                <div class="row">

            <div class="col-12">

                <div id="dashboard-card" class="card">

                    <div class="card-body">

                        Weekaanbieding

                    </div>

                </div>

            </div>

        </div>

        <br />

    </div>

@endsection
