@extends('public.layout.structure.public-app-store')

@section('content')

    <div class="container">

        <div class="row">

            <div class="col-12">

                <div id="dashboard-card" class="card">

                    <div class="card-body">

                        @include('parts.productslider')

                    </div>

                </div>

            </div>

        </div>

        <div class="row">

                    @include('logged.page.dashboard.navigation')

            <div class="col-8">

                <div id="dashboard-card" class="card">
                    <div class="card-body">

                            <h4 class="title">Persoonlijke gegevens</h4>

                            <form action="/">

                            <div class="row details-form-row">

                                <div class="col-6">

                                    <div class="form-group">
                                        <label for="companyname">Bedrijfsnaam</label>
                                        <input class="form-control input-sm" name="companyname" value="{{ Auth::user()->companyname }}">
                                    </div>     

                                    <div class="form-group">
                                        <label for="firstname">Voornaam</label>
                                        <input class="form-control input-sm" name="firstname" value="{{ Auth::user()->firstname }}">
                                    </div>     

                                    <div class="form-group">
                                        <label for="lastname">Achternaam</label>
                                        <input class="form-control input-sm" name="lastname" value="{{ Auth::user()->lastname }}">
                                    </div>     

                                    <div class="form-group">
                                        <label for="email">E-mail adres:</label>
                                        <input class="form-control input-sm" name="email" value="{{ Auth::user()->email }}">
                                    </div>    
{{-- 
                                    <div class="form-group">
                                        <label for="lastname">Finance e-mail adres:</label>
                                        <input class="form-control input-sm" name="lastname" value="{{ Auth::user()->lastname }}">
                                    </div>     --}}


                                </div>
                                <div class="col-6">

                                    <div class="form-group">
                                        <label for="companynumber">KvK nummer:</label>
                                        <input class="form-control input-sm" name="companynumber" value="{{ Auth::user()->companynumber }}">
                                    </div>  

                                    <div class="form-group">
                                        <label for="vatnumber">BTW nummer:</label>
                                        <input class="form-control input-sm" name="vatnumber" value="{{ Auth::user()->vatnumber }}">
                                    </div>  

                                </div>

                            </div>

                                <div class="row">

                                    <div class="col-3">

                                        <button type="submit" class="btn btn-success">Gegevens opslaan</button>
                                    
                                    </div>

                                </div>

                        </form>
    
                    </div>

                </div>

            </div>


        </div>

    </div>

@endsection
