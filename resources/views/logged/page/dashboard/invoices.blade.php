@extends('public.layout.structure.public-app-store')

@section('content')

    <div class="container">

        <div class="row">

            <div class="col-12">

                <div id="dashboard-card" class="card">

                    <div class="card-body">

                        Advertenties

                    </div>

                </div>

            </div>

        </div>

        <div class="row">

                    @include('logged.page.dashboard.navigation')

            <div class="col-8">

                <div id="dashboard-card" class="card">
                    <div class="card-body">

                            <span class="invoice-pay-message">LET OP!</span> <br />

                             Er zijn een of meer verlopen facturen ter waarde van <b>€ 322,32</b>. 

                            <a class="btn btn-bekijk pull-right" href="#">Betaal nu</a> 
    
                    </div>

                </div>

                <div id="dashboard-card" class="card">
                    <div class="card-body">

                            <h4 class="title">Facturen betalen</h4>

                            <p>U vindt hier een overzicht van uw openstaande en reeds betaalde facturen.</p>
    
                    </div>

                </div>

                                <div id="dashboard-card" class="card">
                    <div class="card-body">

                            <h4 class="title">Betalingsoverzicht</h4>

                            U vindt hieronder een overzicht van openstaande en reeds betaalde facturen.

                            <div class="payment-header">

                                <div class="row">

                                    <div class="col-2">Factuur</div>
                                    <div class="col-2">Datum</div>
                                    <div class="col-2">Bedrag</div>
                                    <div class="col-2">Verval</div>
                                    <div class="col-2">Status</div>
                                    <div class="col-2">Betalen</div>

                                </div>

                            </div>

                            <div class="payment-rule">

                                <div class="row payment-row">

                                    <div class="col-2">#2017-1021</div>
                                    <div class="col-2">27-06-2018</div>
                                    <div class="col-2">€ 322,32</div>
                                    <div class="col-2">07-07-2018</div>
                                    <div class="col-2 payment-row-status not-paid">Verlopen</div>
                                    <div class="col-2"><span class="fas payment-row-pay fa-credit-card"></span></div>

                                </div>

                            </div>
        
                    </div>
                </div>

            </div>


        </div>

    </div>

@endsection
