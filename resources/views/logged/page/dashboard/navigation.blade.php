            <div class="col-4">

                <div id="dashboard-card" class="card">
                    <div id="dashboard-navigation" class="card-body">

                      <h3>Finance</h3>

                      <hr />

                        <ul class="nav flex-column">
                          <li class="nav-item">
                            <a class="nav-link active" href="{{ route('creditcontrol') }}"><span class="fas fa-cubes"></span>&nbsp; Op rekening</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link active" href="{{ route('payinvoices') }}"><span class="fas fa-credit-card"></span>&nbsp; Facturen betalen</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link active" href="#"><span class="fas fa-credit-card"></span>&nbsp; Inkoop analyse</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link active" href="#"><span class="fas fa-credit-card"></span>&nbsp; Betalingsregeling</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link active" href="#"><span class="fas fa-credit-card"></span>&nbsp; Tegoed</a>
                          </li>
                        </ul>    

                    </div>
                </div>

                <div id="dashboard-card" class="card">
                    <div id="dashboard-navigation" class="card-body">

                      <h3>Gegevens & instellingen</h3>

                      <hr />

                        <ul class="nav flex-column">

                          <li class="nav-item">
                            <a class="nav-link active" href="{{ route('addresscontrol') }}"><span class="fas fa-address-card"></span>&nbsp; Adres boek</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link active" href="#"><span class="fas fa-address-card"></span>&nbsp; Winkels / filialen</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link active" href="{{ route('details') }}"><span class="fas fa-building"></span>&nbsp; Bedrijfsgegevens</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link active" href="{{ route('accountcontrol') }}"><span class="fas fa-info"></span>&nbsp; Gebruikers</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link active" href="#"><span class="fas fa-info"></span>&nbsp; Wachtwoord wijzigen</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link active" href="#"><span class="fas fa-info"></span>&nbsp; Privacy</a>
                          </li>

                        </ul>    

                    </div>
                </div>

            </div>

