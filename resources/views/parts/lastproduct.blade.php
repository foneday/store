		<div id="eerder-bekeken">

        @if(!empty($products))

		<h4>Eerder bekeken door jou</h4>

        @endif

		<div class="row">
	
			@foreach($products as $product)
			<div class="col-2">
                    <a href="/product/{{ $product->slug }}">
				<div class="eerderbekeken">
				  <div class="pimages">

                        @if($product->pimage()->get()->isNotEmpty())

                       
                        <img src="{{ asset('images/products/'.$product->pimage()->first()->url.'') }}" class="bekeken-product-image">

           

                        @else

                        <img src="{{ asset('images/products/placeholder.png') }}" class="product-image bekeken-product-image active">

                        @endif

                         </div>

                         <div class="ptext">

                         <h6>{{ $product->name }}</h6>

                         <span class="price">
                                € @if(empty($product->discount)) 
                                 {{ number_format($product->price, 2, ',', '.') }}
                                @else
                                 {{ number_format($product->discount, 2, ',', '.') }}
                                @endif
						</span>

						<span class="discount">

 						@if(!empty($product->discount)) <span class="product-price discount">€ {{ number_format($product->price, 2, ',', '.') }}</span> @endif

 						</span>

                     </div>
                     </div>
			</div>
        </a>
			@endforeach

		</div>

		</div>