            <div id="services-balk">
                <div class="row">
                    <div class="col-3">
                        <span class="fas fa-truck"></span>
                        <span class="content-head">Voor 20:30 uur besteld</span>
                        <span class="content-footer">Morgen al bezorgd</span>
                    </div>
                                        <div class="col-3">
                        <span class="fas fa-book"></span>
                        <span class="content-head">Kwaliteit gegarandeerd</span>
                        <span class="content-footer">Bij al onze producten</span>
                    </div>
                                        <div class="col-3">
                        <span class="fas fa-archive"></span>
                        <span class="content-head">Afhalen op locatie</span>
                        <span class="content-footer">Haal je bestelling direct op</span>
                    </div>
                                        <div class="col-3">
                        <span class="fas fa-at"></span>
                        <span class="content-head">Snelle klantenservice</span>
                        <span class="content-footer">Stel gemakkelijk een vraag</span>
                    </div>
                </div>
            </div>