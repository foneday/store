  @forelse(Cart::content()->sort() as $cart)

    <div class="row">

      <div class="d-flex align-items-center justify-content-center h-50">

      <div class="col-3">

        <img class="cart-item-image" src="{{ $cart->options->image }}">

      </div>

      <div class="col-8">

              <span class="cart-item-title">{{ mb_strimwidth($cart->name, 0, 21, ' …') }}</span> <br />
              @if(!empty($cart->options->discount)) 
              <span class="cart-item-price">€ {{ $cart->options->discount * $cart->qty }}</span> 
              @else
              <span class="cart-item-price">€ {{ $cart->price * $cart->qty }}</span> 
              <span class="cart-item-amount">Aantal: {{ $cart->qty }}</span> 
              @endif

      </div>

        <span data-id="{{ $cart->rowId }}" class="fas remove-from-cart fa-times delete-row"></span>

    </div>

    </div>

    @if($loop->index === 2)
      @break
    @endif

    @empty 

        Uw winkelwagen is leeg.

    @endforelse