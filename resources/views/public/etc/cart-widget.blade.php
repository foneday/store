<div id="cart-dropdown" class="dropdown show">
  <a class="" href="#" id="cartDrop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="far fa-shopping-cart cart-open-icon"><a class="cart-href" href="#"> </a></span>
  </a>

  <div class="dropdown-menu" aria-labelledby="cartDrop">

  <div id="top-cart" class="container">

  	<div class="row">

  		<div class="col-3"><span class="aantal">{{ Cart::count() }}</span></div>
  		<div class="col-9 text-right">Totaal: <span class="total-amount">€ {{ Cart::total() }}</span> </div>

  	</div>

  </div>

  	<hr />

   <div id="center-cart" class="container">

      @include('public.etc.cart-content')

  </div>

  <div id="footer-cart" class="container">

  	<div class="row">

      <div class="col text-center"> <a class="btn btn-cart btn-afrekenen btn-success" href="{{ route('winkelwagen') }}">Afrekenen</a>
 <a class="cart-clear" href="#">Winkelwagen legen</a></div>

  	</div>

  </div>

  </div>
</div>