
<div id="footer">

	<div class="container">

		<div class="row">

			<div class="col-2">

				<h6>Bekende producten</h6> 

				<ul class="list">

					<li><a href="#">Iphone X Display</a></li>
					<li><a href="#">Samsung Galaxy S6 Camera</a></li>
					<li><a href="#">Nokia 310 Front</a></li>
					<li><a href="#">Iphone 6s+ OEM scherm</a></li>
					<li><a href="#">Huawei scherm OEM display</a></li>

				</ul>


			</div>

			<div class="col-2">

				<h6>Over FoneDay</h6> 

				<ul class="list">

					<li><a href="#">Ons verhaal</a></li>
					<li><a href="#">Werken bij FoneDay</a></li>
					<li><a href="#">Vacatures</a></li>

				</ul>


			</div>
			<div class="col-4"></div>

			<div class="col-2 right">

				<h6>Snel navigeren</h6> 

				<ul class="list">

					<li><a href="#">Service</a></li>
					<li><a href="#">Onze zekerheden</a></li>
					<li><a href="#">Nieuw account maken</a></li>

				</ul>


			</div>

			<div class="col-2 right">

				<h6>Hulp en contact</h6> 

				<ul class="list">

					<li><a href="#">Klantenservice</a></li>
					<li><a href="#">Contactgegevens</a></li>
					<li><a href="#">Veelgestelde vragen</a></li>
					<li><a href="#">Livechat</a></li>

					@can('dashboard')

					<li><a id="beheerurl" href="{{ URL('/nova') }}">Beheer</a></li>
					
					@endcan


				</ul>


			</div>

		</div>

		<div class="row">

			<div class="col">

				<span class="small-links">Veiligheidsinformatie - Privacy statement - Cookies - Algemene voorwaarden</span>

				<p class="small-text">FoneDay Online B.V. - Goirkekanaaldijk 211-09, 5048AA Tilburg <br />
			    Btw-nummer: NL858716422B01 - KvK-nummer: 71440836
				</p>

			</div>

		</div>

	</div>

</div>