<nav class="navbar navbar-top navbar-expand-lg navbar-light">
  <div class="container">

    <ul class="navbar-nav mr-auto"></span><span class="fas fa-check"></span>&nbsp; De klantenservice is op dit moment geopend, neem nu contact op.</ul>

    <div class="dropdown show">
      <a class="" href="#" id="mijnFoneDayLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
        <ul class="navbar-nav ml-auto"><span class="fas fa-user"></span>&nbsp; @auth {{ Auth::user()->name }} @endauth @guest Mijn FoneDay @endguest</ul>
      </a>

      <div class="dropdown-menu" id="mijnFoneDayMenu" aria-labelledby="mijnFoneDayLink">

        @auth

        <a class="dropdown-item dropdown-mijnfoneday" href="/producten"><span class="fas fa-archive"></span> &nbsp; Ga naar webshop</a>
        <a class="dropdown-item dropdown-mijnfoneday" href="{{ route('dashboard') }}"><span class="fas fa-user"></span> &nbsp; Mijn account</a>
        <a class="dropdown-item dropdown-mijnfoneday" href="{{ route('logout') }}"><span class="fas fa-stop-circle"></span> &nbsp; Uitloggen</a>

        @endauth

        @guest

        <a class="dropdown-item dropdown-mijnfoneday" href="{{ route('login') }}"><span class="fas fa-lock"></span> &nbsp; Inloggen</a>
        <a class="dropdown-item dropdown-mijnfoneday" href="#"><span class="fas fa-info-circle"></span> &nbsp; Meer informatie</a>
        <a class="dropdown-item dropdown-mijnfoneday" href="#"><span class="fas fa-user-circle"></span> &nbsp; Een account aanvragen</a>

        @endguest

      </div>
    </div>

  </div>
</nav>

<nav class="navbar navbar-main navbar-expand-lg navbar-light sticky-top bg-white">
	<div class="container">
  <a class="navbar-brand" href="/"> <img id="logo" src="{{ asset('images/branding/foneday-logo.svg') }}">  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>


  <div class="veilig-verbinding">

    <span class="fas fa-lock"></span> Veilige verbinding

  </div>


</div>
</nav>