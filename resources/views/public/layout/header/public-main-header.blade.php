<nav class="navbar navbar-top navbar-expand-lg navbar-light">
  <div class="container">

    <ul class="navbar-nav mr-auto"></span></ul>

      <a class="" href="#"> 
        <ul class="navbar-nav ml-auto top-link">Klantenservice</span></ul>
      </a>

    <div class="dropdown show">
      <a class="" href="#" id="mijnFoneDayLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
        <ul class="navbar-nav ml-auto"><span class="fas fa-user"></span>&nbsp; @auth {{ Auth::user()->name }} @endauth @guest Mijn FoneDay @endguest</ul>
      </a>

      <div class="dropdown-menu" id="mijnFoneDayMenu" aria-labelledby="mijnFoneDayLink">

        @auth

        <a class="dropdown-item dropdown-mijnfoneday" href="/producten"><span class="fas fa-archive"></span> &nbsp; Ga naar webshop</a>
        <a class="dropdown-item dropdown-mijnfoneday" href="{{ route('dashboard') }}"><span class="fas fa-user"></span> &nbsp; Mijn account</a>
        <a class="dropdown-item dropdown-mijnfoneday" href="{{ route('logout') }}"><span class="fas fa-stop-circle"></span> &nbsp; Uitloggen</a>

        @endauth

        @guest

        <a class="dropdown-item dropdown-mijnfoneday" href="{{ route('login') }}"><span class="fas fa-lock"></span> &nbsp; Inloggen</a>
        <a class="dropdown-item dropdown-mijnfoneday" href="#"><span class="fas fa-info-circle"></span> &nbsp; Meer informatie</a>
        <a class="dropdown-item dropdown-mijnfoneday" href="#"><span class="fas fa-user-circle"></span> &nbsp; Een account aanvragen</a>

        @endguest

      </div>
    </div>

  </div>
</nav>

<nav class="navbar navbar-main navbar-expand-lg navbar-light sticky-top bg-white">
	<div class="container">
  <a class="navbar-brand" href="/"> <img id="logo" src="{{ asset('images/branding/foneday-logo.svg') }}">  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav navbar-top-nav mr-auto">
      <li class="nav-item active">
      @auth
        <a class="nav-link" href="/frontpage">Homepagina<span class="sr-only">(current)</span></a>
      @else
        <a class="nav-link" href="/">Homepagina<span class="sr-only">(current)</span></a>
      @endauth
      </li>

      @auth
      <li class="nav-item">
        <a class="nav-link" href="/producten">Ons assortiment</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Klantenservice</a>
      </li>
      @endauth

    </ul>

    @auth

            <form id="custom-search-form" class="form-search form-horizontal pull-right">
                <div class="input-append span12">
                    <input type="text" class="search-query mac-style form-control zoekbalk"  placeholder="Waar ben je naar opzoek?">
                </div>
                <button type="submit" class="btn"><i class="fas fa-search"></i></button>
            </form>

            @endauth

    @auth

        @include('public.etc.cart-widget')

    @endauth

  </div>
</div>
</nav>

@auth

<nav class="navbar navbar-store navbar-expand-lg navbar-light bg-white">
  <div class="container">

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      @foreach($merken as $merk)
      <li class="nav-item">
        <a class="nav-link" href="/producten/{{ $merk->id }}">{{ $merk->name }}</a>
      </li>
      @endforeach
    </ul>
  </div>
</div>
</nav>

@endauth