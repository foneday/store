@extends('public.layout.structure.public-app-store')

@section('content')

<div id="cart-page" class="container">

	<div class="row">

		<div class="col-12">



		</div>

	</div>

	<div class="row">

		<div id="cart-content" class="col-8">

				<div class="cart-header-content">

						<div class="pull-left">

								<h3>Winkelwagen</h3>

						</div>

						<div class="pull-right">

							    @if(Cart::content()->isNotEmpty())

								<a href="/checkout/shipment" class="btn btn-success">Afrekenen</a>

							@endif

						</div>

				</div>

				<div class="cart-product-list">

	@forelse(Cart::content()->sort() as $cart)

					<div class="cart-product">

						<hr />

							<img class="cart-page-image" src="{{ $cart->options->image }}" alt="Product omschrijving">

							<span class="cart-page-product-title">{{ $cart->name }}</span>

							<span class="cart-page-product-price">€ {{ $cart->price * $cart->qty }}</span>

							<span class="cart-page-product-stock">Nog 2 op voorraad</span>

							<span class="cart-page-product-aantal-title">Aantal:</span>

							<span class="cart-page-product-aantal">

								<select class="form-control-sm">
									<option value="1" {{ ( $cart->qty ==  '1') ? 'selected' : '' }}>1</option>
									<option value="2" {{ ( $cart->qty ==  '2') ? 'selected' : '' }}>2</option> 
									<option value="3" {{ ( $cart->qty ==  '3') ? 'selected' : '' }}>3</option>
									<option value="4" {{ ( $cart->qty ==  '4') ? 'selected' : '' }}>4</option>
									<option value="5">5</option>
									<option value="6">6</option>
								</select>

							</span>

							<span class="cart-page-product-delete">
									<a href="/delete-from-cart-wj/{{ $cart->rowId }}"><span class="fas remove-from-cart fa-times"></span></a>
							</span>

						<hr />

					</div>


    @empty 

        Uw winkelwagen is leeg.

    @endforelse

    @if(Cart::content()->isNotEmpty())

    <div class="row">

	
	</div>

    @endif

				</div>


		</div>

		<div class="col-4">

			<div id="cart-side">

					<div id="cart-continue">

						<div class="pull-left">
							Totaal ex btw: <br />
							BTW:
						</div>

						<duv class="pull-right">
							€ {{ number_format(Cart::total(), 2, ',', '.') }}<br />
							€ {{ number_format( (Cart::total() * 1.21 ) - Cart::total(), 2, ',', '.') }} 

						</duv>


					</div>

						<hr />

					<div id="cart-continue">

						<div class="pull-left">
							Totaal:
						</div>

						<duv class="pull-right">
							€ {{ number_format(Cart::total() * 1.21, 2, ',', '.') }} 
						</duv>


					

			</div>
			

		</div>

	</div>
		@include('parts.lastproduct')

		<br />

</div>

@endsection

@section('js')

@endsection

