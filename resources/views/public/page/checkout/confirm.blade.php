@extends('public.layout.checkout.checkout-layout')

@section('content')

		 <div id="checkout" class="container">

            <input class="d-none order_id" value="{{ $order->id }}">

		 	<form action="{{ route('checkoutpayment') }}" method="get">

			<div class="row">

				<div class="offset-2 col-8">
			      <ul class="progressbar">
			          <li class="active">Winkelwagen</li>
			          <li class="active">Verzenden</li>
			          <li class="active">Betalen</li>
			          <li class="active">Bevestiging</li>
			 	 </ul>
			 	</div>

		 	</div>

		 	<div class="row">

		 		<div id="checkout-shipment" class="offset-2 col-8 checkout-confirm">

                <div id="verwerktexttogo" class="verwerktext">

		 			<h5 class="verwerk-header">We zijn uw bestelling aan het verwerken..</h5> <br />
                    <h6>Dit kan enkele seconden in beslag nemen</h6>

                    <br />

                </div>


                <div id="verwerktextdone" class="verwerktext">

                    <h5 class="verwerk-header">We hebben uw bestelling succesvol verwerkt!</h5> <br />
                    <h6>We gaan zo snel mogelijk voor u aan de slag!</h6> <br />
                    <h6>Druk hier om uw bestelling te bekijken:</h6>
                    <a class="btn btn-bekijk-bestelling btn-success" href="#">Bekijk bestelling</a>

                </div>

                    <div class="circle-loader">
  <div class="checkmark draw"></div>
</div>


                    <div id="animationWindow">
</div>

		      	<div><button type="submit" class="checkout-button btn btn-success">Doorgaan naar betaling</button></div>

		 	</div>

		 </form>

	
	</div>

@endsection