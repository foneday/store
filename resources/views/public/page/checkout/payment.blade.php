@extends('public.layout.checkout.checkout-layout')

@section('content')

    <form method="get" action="{{ route('neworder') }}">
    
         <div id="checkout" class="container">

            <div class="row">

				<div class="offset-2 col-8">
			      <ul class="progressbar">
			          <li class="active">Winkelwagen</li>
			          <li class="active">Verzenden</li>
			          <li class="active">Betalen</li>
			          <li>Bevestiging</li>
			 	 </ul>
			 	</div>

		 	</div>

		 	<div class="row">

		 		<div id="checkout-shipment" class="offset-2 col-8">

                    <h5>Betaling</h5>

                    <p>Het door u te betalen bedrag is <span class="betalen-bedrag">  ?</span></p>

                    @if(Auth::user()->credit == false)
                    <p>Op dit moment kunt u niet op rekening bestellen. Om op rekening te bestellen dient u een aanvraag te doen in <a href="{{ route('creditcontrol') }}">uw account beheer</a>. 
                    @endif

                    <h6>Kies een betaalwijze:</h6>

                                            <div class="row form-group product-chooser">
    
        <div class="post paymentknop col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="product-chooser-item">
                <div class="boxedpayment col-xs-8 col-sm-8 col-md-12 col-lg-12">
                    <img class="ideallogo" src="{{ asset('/images/payment/ideal.png') }}">
                    <input type="radio" class="" name="betaalmethode" value="ideal">
                </div>
                <div class="clear"></div>
            </div>
        </div>
        
        <div class="pakketpunt paymentknop col-xs-12 col-sm-12 col-md-4 col-lg-4 choose-shipment">
            <div class="product-chooser-item">
                <span class="big-icon fas fa-box"></span>
                <div class="boxedpayment col-xs-8 col-sm-8 col-md-12 col-lg-12">
                     <img class="paypallogo" src="{{ asset('/images/payment/paypal.png') }}">
                    <input type="radio" name="betaalmethode" value="paypal">
                </div>
                <div class="clear"></div>
            </div>
        </div>

        @if(Auth::user()->credit == true)
        
        <div class="ophalen paymentknop col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="product-chooser-item">
                <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                    <span class="title">Op rekening</span>
                    <span class="description">Betaal uw bestelling later doormiddel van een factuur. </span>
                    <input type="radio" name="betaalmethode" value="krediet">
                </div>
                <div class="clear"></div>
            </div>
        </div>

        @endif

        <input class="d-none" name="bezorgmethode" value="{{ $request->bezorgmethode }}">
        <input class="d-none" name="address" value="{{ $request->address }}">

                </div>

   
                       @if($request->bezorgmethode == 'post')   

                    <h6>Kies een koerier:</h6>

     <div class="row form-group product-chooser">
    
        <div class="post col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="product-chooser-item selected">
                <div class="boxedpayment col-xs-8 col-sm-8 col-md-12 col-lg-12">
                    <img class="postnllogo" src="{{ asset('/images/branding/post-nl-icon.svg') }}">
                    <input type="radio" class="" name="koerier" value="postnl" checked="checked">
                </div>
                <div class="clear"></div>
            </div>
        </div>
        
        <div class="pakketpunt col-xs-12 col-sm-12 col-md-4 col-lg-4 choose-shipment">
            <div class="product-chooser-item">
                <span class="big-icon fas fa-box"></span>
                <div class="boxedpayment col-xs-8 col-sm-8 col-md-12 col-lg-12">
                     <img class="upslogo" src="{{ asset('/images/branding/ups-icon.svg') }}">
                    <input type="radio" name="koerier" value="ups">
                </div>
                <div class="clear"></div>
            </div>
        </div>

    </div>

    @endif


                <a href="{{ route('checkoutshipment') }}" class="checkout-button btn btn-success pull-left" style="display: inline-block;">Vorige stap</a>
                    <button type="submit" class="btn btn-pay btn-success pull-right">Betalen</button>
                </form>
                </div>


            </div>

		 			

	
	</div>

@endsection

@section('js')


@endsection