@extends('public.layout.checkout.checkout-layout')

@section('content')

		 <div id="checkout" class="container">

		 	<form action="{{ route('checkoutpayment') }}" method="get">

			<div class="row">

				<div class="offset-2 col-8">
			      <ul class="progressbar">
			          <li class="active">Winkelwagen</li>
			          <li class="active">Verzenden</li>
			          <li>Betalen</li>
			          <li>Bevestiging</li>
			 	 </ul>
			 	</div>

		 	</div>

		 	<div class="row">

		 		<div id="checkout-shipment" class="offset-2 col-8">

		 			<h5>Bezorgmethode en adres</h5>

		 			<div id="bezorgmethode">

		 			<h6>Kies een bezorgmethode</h6>

		 			    <div class="row form-group product-chooser">
    
    	<div class="post col-xs-12 col-sm-12 col-md-4 col-lg-4">
    		<div class="product-chooser-item">
                <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
    				<span class="title">Bezorgen op locatie</span>
    				<span class="description">Een door u gekozen koerier komt het pakket bij u afleveren.</span>
    				<input type="radio" class="" name="bezorgmethode" value="post">
    			</div>
    			<div class="clear"></div>
    		</div>
    	</div>
    	
    	<div class="pakketpunt col-xs-12 col-sm-12 col-md-4 col-lg-4 choose-shipment">
    		<div class="product-chooser-item">
    			<span class="big-icon fas fa-box"></span>
                <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
    				<span class="title">Ophalen bij pakketpunt</span>
    				<span class="description">Op een door u gekozen pakketpunt kunt u het pakket ophalen. Zodra het pakket kan worden afgehaald krijgt u een afhaalbericht via de mail.</span>
    				<input type="radio" name="bezorgmethode" value="pakketpunt">
    			</div>
    			<div class="clear"></div>
    		</div>
    	</div>
    	
    	<div class="ophalen col-xs-12 col-sm-12 col-md-4 col-lg-4">
    		<div class="product-chooser-item">
                <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
    				<span class="title">Ophalen bij FoneDay</span>
    				<span class="description">U kunt het pakket ophalen bij FoneDay zelf. </span>
    				<input type="radio" name="bezorgmethode" value="ophalen">
    			</div>
    			<div class="clear"></div>
    		</div>
    	</div>

				</div>

		 		</div>


		 	<div id="checkout-adres">

		 			<h6>Kies een adres / bedrijf</h6>
		 				 			    <div class="row form-group product-chooser">

		 				 	
    
		 			@forelse($addresses as $address)

    	<div id="addresspick" class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
    		<div class="product-chooser-item">
                <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
    				<span class="title">{{ $address->name }}</span>
    				<span class="description">{{ $address->streetaddress_1 }} <br />
    				{{ $address->zipcode }} {{ $address->city }}, {{ $address->country }} <br />
					Verzendkosten: 

					@if($address->getVerzendkosten($address->country) == 0)
						Gratis
					@else 
						€ {{ $address->getVerzendkosten($address->country) }}
					@endif
				
    				</span>
    				<input type="radio" name="address" value="{{ $address->id }}">
    			</div>
    			<div class="clear"></div>
    		</div>
    	</div>

    	@empty

    	<p>Er lijken nog geen adressen te zijn toegevoegd, voeg deze eerst toe in uw <a href="{{ route('addresscontrol') }}" target="_blank">persoonlijke dashboard</a>. </p>

    	@endforelse
 

				</div>
		 	</div>

		 	<div id="checkout-ophalen">
				Indien u kiest voor de optie afhalen kunt u de bestelling ophalen bij FoneDay zelf. Zodra je bestelling klaar staat ontvangt u een bericht via de mail. <p></p>
		 	</div>

		 	<div id="checkout-pickup">

				U wordt zo doorgeleidt naar het pakketpunt selectie portaal. U kunt hier een locatie selecteren bij u in de buurt. <p></p>
		 	</div>

		 	<div><button type="submit" class="checkout-button btn btn-success">Doorgaan naar betaling</button></div>

		 	</div>

		 </form>

	
	</div>

@endsection

@section('js')


@endsection