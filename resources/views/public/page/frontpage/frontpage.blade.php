@extends('public.layout.structure.public-app-store')

@section('content')

<div id="products-page" class="container">


			@include('parts.servicebalk')

			@include('parts.productslider')

			<div id="sales">

		</div>


			<div id="blocks" class="row">

				<div class="col-3 first">

					<img class="first-image" src="{{ asset('images/branding/blokgratisverzending.png') }}">

				</div>

				<div class="col-3">
					<a href="/producten/1">
						<img src="{{ asset('images/branding/iphoneproducten.png') }}">
					</a>
				</div>

				<div class="col-3">
					<a href="/producten/3">
						<img src="{{ asset('images/branding/huaweiproducten.png') }}">
					</a>
				</div>

				<div class="col-3 last">
					<a href="/producten/2">	
						<img src="{{ asset('images/branding/samsungproducten.png') }}">
					</a>
				</div>

		</div>

			<div id="firstrow" class="row">

				<div class="col-8">

					<div class="buttonsblok">

						<a href="{{ route('dashboard') }}" class="btn btn-dashboard">Ga naar uw persoonlijke dashboard</a>

					</div>

					<br />

					<div class="topblok">

						<h4>Aanbieding van de week</h4>
						<span class="aanbieding-ondertitel">Elke week nieuwe aanbiedingen bij FoneDay</span>

					</div>

				</div>

				<div class="col-4">

					<div class="accountmanager">

						<h4>Wij denken met u mee</h4>
						<span class="aanbieding-ondertitel">Uw persoonlijke accountmanager staat altijd voor u klaar. Heeft u vragen of problemen, twijfel dan niet om contact op te nemen.  </span>

						<img src="{{ asset('images/sergio.png') }}">

					</div>

				</div>

		</div>

		<div id="eerderbekekendiv">

		@include('parts.lastproduct')

	</div>


	</div>

	<div id="balkpluscontainer" class="container-fluid">
		<div id="balkplus" class="container">
			<div class="row">
			<div class="col-4"><span class="fas fa-angle-right"></span> Voor 20:30 besteld, de volgende dag geleverd</div>
			<div class="col-4"><span class="fas fa-angle-right"></span> Een uitgebreid assortiment van producten</div>
			<div class="col-4"><span class="fas fa-angle-right"></span> Wekelijks wisselende aanbiedingen	</div>
			</div>
		</div>

	</div>



</div>

<br />

@endsection

@section('js')

@endsection

