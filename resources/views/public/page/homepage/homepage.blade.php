@extends('public.layout.structure.public-app-head')

@section('content')

<div class="container-fluid" id="container-fluid-video">

			<div class="overlay">

			</div>

</div>

<div id="featured-products"> 
	<div class="container">

		<br />

				<h3>Nu bij FoneDay</h3>

				<h3 class="thin-title">De beste producten met de beste service</h3>

		<div class="row">
			<div class="col col-centered text-center">

				<img class="featured-photo" src="{{ asset('images/example/samsung_1.jpg') }}">

				<div class="featured-title">Samsung (N950) Display</div>

				<div class="featured-second-title">(LCD & Touchscreen) Gold</div>

				<a class="featured-button btn btn-success">Bekijk Samsung (N950) Display</a>


			</div>
			<div class="col col-centered text-center">

				<img class="featured-photo" src="{{ asset('images/example/samsung_2.jpg') }}">

				<div class="featured-title">Iphone X Display</div>

				<div class="featured-second-title">(LCD & Touchscreen) Black [OEM]</div>

				<a class="featured-button btn btn-success">Bekijk Iphone X Display</a>


			</div>
			<div class="col col-centered text-center">

				<img class="featured-photo" src="{{ asset('images/example/ipad.jpg') }}">

				<div class="featured-title">Apple iPad Pro 12.9 Display</div>

				<div class="featured-second-title">(Complete with Mainboard) Black [2017]</div>

				<a class="featured-button btn btn-success">Bekijk Apple iPad Pro 12.9 Display</a>


			</div>
		</div>
	</div>
</div>

<div id="beleef-foneday"> 
	<div class="container">

		<br />

				<h3>Beleef FoneDay</h3>

				<h3 class="thin-title">Hoe kunnen wij jou van dienst zijn?</h3>


		<div class="row">
			<div class="col-4 col-centered">

				<img class="beleef-image" src="https://www.kpn.com/upload_mm/9/2/3/2095621_1532695167893_pride_homepage_mbsdelenArtboard-1.png">

				<h5>Innovatie</h5>

Stilstaan staat niet in onze vocabulaire. Dankzij het optimaliseren en automatiseren van onze bedrijfsprocessen, zijn wij onze klanten snel en efficiënt van dienst. Dit heeft geleid tot een explosieve groei van Foneday. De grip op processen leidt ertoe dat wij de focus kunnen leggen op u als klant. We zijn er dus om u optimaal te servicen en te realiseren wat u nodig heeft.

			</div>
			<div class="col-4 col-centered">

				<img class="beleef-image" src="https://www.kpn.com/upload_mm/d/8/2/2095595_1532695036177_homepage-blog-midden.png">

				<h5>Ontzorgen en lage prijzen</h5>

Wij als leverancier van smartphone- en tabletonderdelen ontzorgen u, zodat u zich kunt richten op het ondernemen en uw belangrijkste brandstof: groei. Wij zorgen ervoor dat u op de hoogte bent van innovaties in ons werkveld. Zo behoudt ieder zijn belangrijkste focus en staat de tijd nooit stil.

Bij Foneday kunt u rekenen op belangrijke garanties. Zo bieden wij u o.a. de laagste prijzen, optimale kwaliteit, goede service en een enorm assortiment. Kortom: op naar een prettige samenwerking!

			</div>
			<div class="col-4 col-centered">


				<img class="beleef-image" src="https://www.kpn.com/upload_mm/b/5/e/2095605_1532695128550_homepage-blog-rechts.png">

				<h5>Voor de zakelijke markt</h5>

Foneday is de groothandel in smartphone- en tabletonderdelen, die enkel levert aan bedrijven. Wilt u van onze diensten profiteren, maak dan een account aan. Binnen 24 uur voeren wij een controle uit en ontvangt u bericht. Vervolgens informeert een accountmanager u over wat wij voor u kunnen betekenen.


			</div>
		</div>

	</div>
</div>

<div id="convert-balk" class="d-flex justify-content-center align-items-center">

	<div class="container">

		<div class="row">

		<div class="col-6">

		<h3>Bezoek nu onze webshop</h3> <br />
		<h5>En bekijk ons volledige aanbod aan producten</h5>

	     </div>

	     <div class="col-3 offset-md-2"><a href="#" class="btn btn-convert">Nu bekijken</a></div>

	     </div>

	</div>

</div>


@endsection
