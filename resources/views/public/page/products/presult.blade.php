@foreach($products as $product)         

                <div class="col-md-4 product-col">

                    <div class="product">

                     @if($product->hasVariation())
                     
                        <div class="quality-{{ $product->pvariation()->first()->quality }} op"></div>

                     @else

                        <div class="quality-{{ $product->quality }} op"></div>

                     @endif 


                    @if($product->hasVariation())

                        <div class="pimages pimages_variation">

                        @if($product->pvariation()->first()->pimage()->get()->isNotEmpty())

                          <a class="prev">&#10094;</a>
                          <a class="next" data-count="1">&#10095;</a>

                        @foreach($product->pvariation()->first()->pimage()->get() as $pimage)

                          <img src="{{ asset('images/products/'.$pimage->url.'') }}" class="product-image {{ $loop->first ?  'active first' : '' }} {{ $loop->last ?  'last' : '' }}">

                        @endforeach

                        @else

                        <img src="{{ asset('images/products/placeholder.png') }}" class="product-image active">

                        @endif

                         </div>

                      @else

                        <div class="pimages">

                        @if($product->pimage()->get()->isNotEmpty())

                          <a class="prev">&#10094;</a>
                          <a class="next" data-count="1">&#10095;</a>

                        @foreach($product->pimage()->get() as $pimage)

                        <img src="{{ asset('images/products/'.$pimage->url.'') }}" class="product-image {{ $loop->first ?  'active first' : '' }} {{ $loop->last ?  'last' : '' }}">

                        @endforeach

                        @else

                        <img src="{{ asset('images/products/placeholder.png') }}" class="product-image active">

                        @endif

                         </div>

                      @endif

                        <hr />

                        <div class="product-content">

                            <div class="product-stars">
                                    <span class="star star-5"></span> (3)<br />
                            </div>
                            
                            @if($product->hasVariation())

                                <div class="product-color">

                                    @foreach($product->pvariation()->get() as $pvariation)

                                    <div data-variationid="{{ $pvariation->id }}" style="background-color: #{{ $pvariation->pcolor()->first()->code }}" class="color {{ $loop->first ?  'picked' : '' }}"></div>

                                    @endforeach

                                </div>

                            @endif

                            <span class="product-brand">{{ $product->brand->name }}</span>  

                            @if($product->hasVariation())

                                @if(!empty($product->pvariation()->first()->discount)) <span class="product-price discount">€ {{ number_format($product->pvariation()->first()->price, 2, ',', '.') }}</span> @endif

                            @else

                                @if(!empty($product->discount)) <span class="product-price discount">€ {{ number_format($product->price, 2, ',', '.') }}</span> @endif

                            @endif

                            <br />

                            <span class="product-title">{{ mb_strimwidth($product->name, 0, 23, '…') }}</span>

                            <span class="product-price normal-price">


                             @if($product->hasVariation())

                                € @if(empty($product->pvariation()->first()->discount)) 
                                 {{ number_format($product->pvariation()->first()->price, 2, ',', '.') }}
                                @else
                                 {{ number_format($product->pvariation()->first()->discount, 2, ',', '.') }}
                                @endif

                             @else

                                € @if(empty($product->discount)) 
                                 {{ number_format($product->price, 2, ',', '.') }}
                                @else
                                 {{ number_format($product->discount, 2, ',', '.') }}
                                @endif

                            @endif

                            </span> 

                             <br />
                         <br />

                        </div>

                        <hr />

                        <div id="product-nav">

                           <span><a class="btn btn-bekijk" href="/product/{{ $product->slug }}">Product bekijken</a></span>
                           <span><input class="amount" value="1" type="number" name="amount"></span>

                           <span><a @if($product->hasVariation()) data-sku="{{ $product->pvariation()->first()->sku }}" @else data-sku="{{ $product->sku }}" @endif class="btn btn-add-cart" href="#"><span class="fas fa-shopping-cart"></span></a></span>

                        </div>

                    </div>

                </div>

@endforeach

<div id="paginatie">

    <div class="row">
        <div class="col-12">
            {!! $products->render() !!}
        </div>
    </div>

</div>