@extends('public.layout.structure.public-app-store')

@section('content')

<div id="products-page" class="container">

	<div id="product-page">

		<div class="row">


	      <!-- Left Column / Headphones Image -->
      <div class="left-column col-7">
      	<div class="whiteboxed picture-boxed">
                    @if($product->hasVariation())

                        <div class="pimages pimages_variation">

                        @if($product->pvariation()->first()->pimage()->get()->isNotEmpty())

                          <a class="prev">&#10094;</a>
                          <a class="next" data-count="1">&#10095;</a>

                        @foreach($product->pvariation()->first()->pimage()->get() as $pimage)

                          <img src="{{ asset('storage/'.$pimage->url.'') }}" class="product-image {{ $loop->first ?  'active first' : '' }} {{ $loop->last ?  'last' : '' }}">

                        @endforeach

                        @else

                        <img src="{{ asset('images/products/placeholder.png') }}" class="product-image active">

                        @endif

                         </div>

                      @else

                        <div class="pimages">

                        @if($product->pimage()->get()->isNotEmpty())

                          <a class="next nt" data-count="1">&#10095;</a>

                        @foreach($product->pimage()->get() as $pimage)

                        <img src="{{ asset('storage/'.$pimage->url.'') }}" class="product-image {{ $loop->first ?  'active first' : '' }} {{ $loop->last ?  'last' : '' }}">

                        @endforeach

                        @else

                        <img src="{{ asset('images/products/placeholder.png') }}" class="product-image active">

                        @endif

                         </div>

                      @endif

                  </div>

                 <div class="whiteboxed omschrijving">
					<h5>Omschrijving</h5>

					<p> {{ $product->description }} </p>
                  </div>


      </div>

      <!-- Right Column -->
      <div class="right-column col-5">

        <!-- Product Description -->
        <div class="product-description">
          <span>{{ $product->brand->name }}</span>
          <h1>{{ $product->name }}</h1>
          <p>{{ $product->short_description }}</p>
        </div>

        <!-- Product Configuration -->
        <div class="product-configuration">

		@if($product->hasVariation())
          <!-- Product Color -->
          <div class="product-color">
            <span>Kleur</span>

            <div class="color-choose">



                                    @foreach($product->pvariation()->get() as $pvariation)


                <div>
                <input data-image="{{ $pvariation->pcolor()->first()->name }}" type="radio" id="{{ $pvariation->pcolor()->first()->name }}" name="color" value="{{ $pvariation->pcolor()->first()->name }}">
                <label for="{{ $pvariation->pcolor()->first()->name }}"><span  style="background-color: #{{ $pvariation->pcolor()->first()->code }}"></span></label>
              </div>

                                    @endforeach

            </div>

          </div>

          @endif

          <!-- Cable Configuration -->
          <div class="cable-config">

				<span>Extra opties</span> 

            <div class="cable-choose">
              <button>Optie</button>
              <button>Optie</button>
              <button>Optie</button>
            </div> 

                        <span>Aantal:</span> 

         		<select class="ammount form-control-sm">
					<option value="1">1</option>
					<option value="2">2</option> 
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
				</select> 

				<br />

            <a href="#">Bekijk nu hoe dit product moet worden gemonteerd</a>
          </div>
        </div>

        <!-- Product Pricing -->
        <div class="product-price">


							<span>
                               @if($product->hasVariation())

                                € @if(empty($product->pvariation()->first()->discount)) 
                                 {{ number_format($product->pvariation()->first()->price, 2, ',', '.') }}
                                @else
                                 {{ number_format($product->pvariation()->first()->discount, 2, ',', '.') }}
                                @endif

                             @else

                                € @if(empty($product->discount)) 
                                 {{ number_format($product->price, 2, ',', '.') }}
                                @else
                                 {{ number_format($product->discount, 2, ',', '.') }}
                                @endif

                            @endif
                        </span>


                                     @if($product->hasVariation())

                                @if(!empty($product->pvariation()->first()->discount)) <span class="product-price discount">€ {{ number_format($product->pvariation()->first()->price, 2, ',', '.') }}</span> @endif

                            @else

                                @if(!empty($product->discount)) <span class="product-price discount">€ {{ number_format($product->price, 2, ',', '.') }}</span> @endif

                            @endif

          <a href="#" data-sku="{{ $product->sku }}" class="cart-btn btn-add-cart">Toevoegen</a>
        </div>
      </div>

	</div>

</div>

</div>


<br />


@endsection

@section('js')

	<script src="{{ asset('js/product.js') }}" defer></script>

@endsection

