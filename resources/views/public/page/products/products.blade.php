@extends('public.layout.structure.public-app-store')

@section('content')

<div id="products-page" class="container">

	<div class="row">

		<div id="filter" class="col-sm-3">

			<div class="filter-content">

				<h4>Filters</h4>

				<hr />

				<form method="get" action="/producten">

				<div id="merken">

					<div class="row">
						<div class="col-6">
							<h5>Merken</h5>
						</div>
						<div class="col-6 text-right">
							  <a class="" data-toggle="collapse" href="#merkenDrop" role="button" aria-expanded="false" aria-controls="merkenDrop">
 <span aria-hidden="true" class="fa fas"></span>
  </a>
						</div>
					</div>
					 
					<div class="collapse.show" id="merkenDrop">

						@foreach($brands as $brand)

							<div class="filter-merk">
									<div class="custom-control custom-checkbox">
									  <input type="checkbox" @if($brand->id == $brandid) checked="checked" @endif class="brand-filter custom-control-input" name="merk" class="merkfilter" data-id="{{ $brand->id }}" id="{{ $brand->name }}" selected>
									  <label class="custom-control-label" for="{{ $brand->name }}">{{ $brand->name }}</label>
									</div>
									<div class="filter-aantal"><span class="filter-aantal-number">({{ $brand->products->count() }})</span></div>
							</div>

						@endforeach
					 
					</div>


				</div>

				<div id="kleuren">

					<div class="row">
						<div class="col-6">
							<h5>Kleuren</h5>
						</div>
						<div class="col-6 text-right">
							  <a class="" data-toggle="collapse" href="#kleurenDrop" role="button" aria-expanded="false" aria-controls="kleurenDrop">
 <span aria-hidden="true" class="fa fas"></span>
  </a>
						</div>
					</div>
					 
					<div class="collapse.show" id="kleurenDrop">

						@foreach($colors as $color)

						<div class="filter-merk">
								<div class="custom-control custom-checkbox">
								  <input type="checkbox" data-id="{{ $color->id }}" class="custom-control-input color-filter" id="{{ $color->name }}">
								  <label class="custom-control-label" for="{{ $color->name }}">{{ $color->name }}</label>
								  <span style="color: #{{ $color->code }}" class="fas fa-circle"></span>
								</div>
								<div class="filter-aantal"><span class="filter-aantal-number">(32)</span></div>
						</div>

						@endforeach

					</div>


				</div>

								<div id="kleuren">

					<div class="row">
						<div class="col-6">
							<h5>Kwaliteit</h5>
						</div>
						<div class="col-6 text-right">
							  <a class="" data-toggle="collapse" href="#kleurenDrop" role="button" aria-expanded="false" aria-controls="kleurenDrop">
 <span aria-hidden="true" class="fa fas"></span>
  </a>
						</div>
					</div>
					 
					<div class="collapse.show" id="kleurenDrop">

			

						<div class="filter-merk">
								<div class="custom-control custom-checkbox">
								  <input type="checkbox" data-id="oem" class="custom-control-input quality-filter" id="oem">
								  <label class="custom-control-label" for="oem">OEM</label>
								</div>
								<div class="filter-aantal"><span class="filter-aantal-number">(32)</span></div>
						</div>
						<div class="filter-merk">
								<div class="custom-control custom-checkbox">
								  <input type="checkbox" data-id="aftermarket" class="custom-control-input quality-filter" id="aftermarket">
								  <label class="custom-control-label" for="aftermarket">Aftermarket</label>
								</div>
								<div class="filter-aantal"><span class="filter-aantal-number">(32)</span></div>
						</div>
						<div class="filter-merk">
								<div class="custom-control custom-checkbox">
								  <input type="checkbox" data-id="genuine" class="custom-control-input quality-filter" id="genuine">
								  <label class="custom-control-label" for="genuine">Genuine</label>
								</div>
								<div class="filter-aantal"><span class="filter-aantal-number">(32)</span></div>
						</div>



					</div>


				</div>



								<div id="prijs-slider">

					<div class="row">
						<div class="col-6">
							<h5>Prijs</h5>
						</div>
						<div class="col-6 text-right">
							  <a class="" data-toggle="collapse" href="#prijsSlider" role="button" aria-expanded="false" aria-controls="prijsSlider">
 <span aria-hidden="true" class="fa fas"></span>
  </a>
						</div>
					</div>
					 
					<div class="collapse.show" id="prijsSlider">

						
						  <div class="form-group">
						    <label for="formControlRange">Example Range input</label>
						    <input type="range" class="form-control-range" id="formControlRange">
						  </div>

					 
					</div>

				</div>




			</div>

		</div>

		<div id="products" class="col-sm-9">


			<div id="sorting">
				<select class="sorting-form form-control form-control">
				  	<option>Sorteren</option>
				  	<option name="prijsop">Prijs oplopend</option>
				  	<option name="prijsaf">Prijs aflopend</option>
				</select>
			</div>

			<input id="searchinput" class="form-control" placeholder="Filter producten.." name="search">

			<div id="results">

				

			</div>


			{{-- <div id="switches">
				<a href="#" class="switch switch-1"><span class="fas fa-th-list"></span></a>
				<a href="#" class="switch switch-2"><span class="fas fa-th-large"></span></a>
				<a href="#" class="switch switch-3"><span class="fas fa-th"></span></a>
			</div> --}}

			<div id="content-row" class="row">

				@include('public.page.products.presult')

			</div>

	
				</div>

			


			</div>

		</div>

	</div>	

</div>

<br />

@endsection

@section('js')

<script src="{{ asset('js/product.js') }}" defer></script>

@endsection

