
<?php

Route::get('/', function () {
    return view('public.page.homepage.homepage');
})->name('homepage');

// Auth routes

Route::post('/inloggen', 'Auth\LoginController@login')->name('login');

Route::get('/inloggen', function () {
	return view('auth.inloggen');
});

Route::post('/login/user', 'CustomLoginController@loginUser')->name('loginuser');

Route::get('/uitloggen', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');

//////////////////////
// Dashboard routes //
//////////////////////

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::get('/dashboard/krediet', 'CreditAanvraagController@credit')->name('creditcontrol');

Route::post('/dashboard/krediet', 'CreditAanvraagController@requestCredit')->name('creditaanvraag');


Route::get('/dashboard/facturen', 'DashboardController@invoices')->name('payinvoices');

Route::get('/dashboard/gegevens', 'DetailsController@details')->name('details');

Route::post('/dashboard/gegevens', 'DetailsController@update')->name('updatedetails');

Route::get('/dashboard/accountbeheer', 'AccountManagerController@index')->name('accountcontrol');

Route::get('/dashboard/adresbeheer', 'AddressController@index')->name('addresscontrol');

Route::post('/dashboard/adresbeheer/delete', 'AddressController@delete')->name('deleteaddress');

Route::post('/dashboard/adresbeheer/status', 'AddressController@switchStatus')->name('switchstatus');

Route::post('/dashboard/adresbeheer/new', 'AddressController@create')->name('newadres');

// Producten routes

Route::get('/producten/{brand?}',array('as'=>'ajax.pagination','uses'=>'ProductController@showProducts'))->name('showproducts');

Route::post('/producten',array('as'=>'ajax.pagination','uses'=>'ProductController@showProducts'))->name('showproducts');

Route::get('/product/{slug}', 'ProductController@show')->name('showproduct');

Route::get('/getcolordata/{id}', 'ProductController@getColorData')->name('getcolordata');

// Cart routes

Route::get('/cart', 'CartController@overview')->name('cart');

Route::post('/add-to-cart', 'CartController@addToCart')->name('addtocart');

Route::post('/delete-from-cart', 'CartController@deleteFromCart')->name('deletefromcart');

Route::get('/delete-from-cart-wj/{id}', 'CartController@deleteFromCartWA');

Route::post('/clearcart', 'CartController@clearCart')->name('clearcart');

Route::get('/getcartcontent', 'CartController@getcartcontent')->name('getcartcontent');

Route::get('/winkelwagen', 'CartController@index')->name('winkelwagen');

// Front page

Route::get('/frontpage', 'FrontController@index')->name('frontpage');

// Checkout process

Route::get('/checkout/shipment', 'CheckoutController@shipment')->name('checkoutshipment');

Route::get('/checkout/payment', 'CheckoutController@payment')->name('checkoutpayment');

Route::get('/checkout/confirmation/{id}', 'CheckoutController@confirmation')->name('confirmation');

// Order routes


Route::get('/checkout/pay', 'OrderController@newOrder')->name('neworder');

Route::post('/checkorder/{id}', 'OrderController@checkorder')->name('ordercheck');

Route::post('/checkout/webhook', 'OrderController@webhook')->name('paymentwebhook');

use App\Shipment;

Route::get('/test2', function() {

$shipment = Shipment::getStatus('14858387');

echo $shipment;

});