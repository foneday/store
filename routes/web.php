<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require_once "public.php"; // Publieke routes

require_once "admin.php"; // Admin routes

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/test', function() {
	return view('public.welcome');
});

Route::get('/dd', 'ProductHistoryController@test');

Nova::routes();

Route::post('/fileupload', 'ImportController@import')->name('importproducts');
Route::get('/fileupload', 'ImportController@form')->name('importproductsform');