<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/inloggen')
                    ->type('email', 'contact@steensolutions.nl')
                    ->type('password', 'secret')
                    ->click('.inloggen')
                    ->pause(1000)
                    ->assertPathIs('/dashboard');

            $browser->visit('/inloggen')
                    ->type('email', 'contact@steensolutions.nl')
                    ->type('password', 'fout')
                    ->click('.inloggen')
                    ->pause(1000)
                    ->assertPathIs('/inloggen')
                    ->assertSee('Deze gegevens kunnen');
        });
    }
}
