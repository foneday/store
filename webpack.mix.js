let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.sass('resources/assets/sass/public/public-app.scss', 'public/css/public'); // Publiek app SCSS bestand

mix.sass('resources/assets/sass/public/public-login.scss', 'public/css/public'); // Publiek login SCSS bestand

mix.js('resources/assets/js/dashboard.js', 'public/js');

mix.js('resources/assets/js/product.js', 'public/js');

mix.js('resources/assets/js/cart.js', 'public/js');

mix.js('resources/assets/js/checkout.js', 'public/js');

mix.copy('node_modules/slick-carousel/slick', 'public/assets/slick');

// Laravel Mix automaticsche browser reloading
mix.browserSync('winkel.app');
